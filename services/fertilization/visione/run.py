#!/usr/bin/env python
import django
import glob
import os
import subprocess
import sys

from django.db import connections
from django.core.management import call_command
from django.core.management.base import CommandParser

from opentelemetry import trace
from opentelemetry.exporter.zipkin.json import Protocol
from opentelemetry.exporter.zipkin.json import ZipkinExporter
from opentelemetry.instrumentation.django import DjangoInstrumentor
from opentelemetry.instrumentation.psycopg2 import Psycopg2Instrumentor
from opentelemetry.propagate import set_global_textmap
from opentelemetry.propagators.b3 import B3SingleFormat
from opentelemetry.sdk.resources import SERVICE_NAME, Resource
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from opentelemetry.sdk.trace.sampling import ParentBased, TraceIdRatioBased

from pathlib import PurePath
from psycopg2 import sql

HOST = os.environ.get("HOST", "0.0.0.0")
PORT = os.environ.get("PORT", 8000)


def collectstatic(args):
    call_command("collectstatic", "--clear", "--no-input")


def init(args):
    import_directory = args.import_directory
    files = glob.glob(
        os.path.join(
            import_directory,
            "*.csv",
        )
    )
    files.sort()
    tables = [os.path.basename(f).split(".")[1] for f in files]

    if not files:
        print("No CSV file found !")
        return

    if args.clear:
        with connections[args.db_alias].cursor() as cursor:
            r = sql.SQL(
                "TRUNCATE {} CASCADE".format(",".join([f"public.{t}" for t in tables]))
            )
            cursor.execute(r)
            print("TRUNCATE {} OK".format(tables))

    for file, table in list(zip(files, tables)):
        call_command("import-copy", table, file, db_alias=args.db_alias)
        print("COPY {} OK".format(os.path.basename(file)))


def runserver(args):
    # Run Django migrations in a subprocess to use settings configured
    # with a Postgres superuser
    manage_py = PurePath(__file__).parent / "manage.py"
    subprocess.run(
        [
            "python",
            manage_py,
            "migrate",
            "--database=default",
            "--settings=config.settings_migration",
        ],
        check=True,
    )

    # Setup statics
    call_command("collectstatic", "--clear", "--no-input")

    # Tracing instrumentation
    os.environ.setdefault("OTEL_PYTHON_DJANGO_INSTRUMENT", "True")

    zipkin_exporter = ZipkinExporter(
        version= Protocol.V2,
        endpoint=os.environ.get(
            'OPENTELEMETRY_EXPORTER_ZIPKIN_ENDPOINT', 'http://127.0.0.1:9411/api/v2/spans'),
    )

    sampler = TraceIdRatioBased(
        float(os.environ.get('OPENTELEMETRY_SAMPLING_RATIO', 0.1))
    )

    trace.set_tracer_provider(
        TracerProvider(
                resource=Resource.create(
                    {
                        SERVICE_NAME: os.environ.get(
                            'OPENTELEMETRY_SERVICE_NAME', 'fertilization-visione')
                    }
                ),
                sampler=ParentBased(sampler)
            )
    )

    trace.get_tracer_provider().add_span_processor(
        BatchSpanProcessor(zipkin_exporter)
    )

    set_global_textmap(B3SingleFormat())

    DjangoInstrumentor().instrument()
    Psycopg2Instrumentor().instrument()

    # Debuging instrumentation
    os.environ.setdefault("DJANGO_REMOTE_DEBUG", "False")
    if os.environ.get("DJANGO_REMOTE_DEBUG").upper() == "TRUE":
        import debugpy
        debugpy.listen(5678)

    call_command("runserver", "{}:{}".format(HOST, PORT), "--noreload")


def main():
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
    django.setup()

    parser = CommandParser()
    subcommand_parser = parser.add_subparsers(
        dest="subcommand",
        required=True,
        title="subcommands",
    )

    collectstatic_parser = subcommand_parser.add_parser(
        "collectstatic", help="collect Django static files"
    )
    collectstatic_parser.set_defaults(func=collectstatic)

    init_parser = subcommand_parser.add_parser(
        "init", help="initialize Django with data"
    )
    init_parser.add_argument(
        "import_directory",
        metavar="import-directory",
        nargs="?",
        help="directory with the files to be imported (default: 'current working directory')",
        type=str,
        default=os.getcwd(),
    )
    init_parser.add_argument(
        "--db-alias",
        help="Django database alias to use (default='default')",
        type=str,
        default="default",
    )
    init_parser.add_argument(
        "--clear",
        help="Truncate tables before importing data",
        action="store_true",
    )
    init_parser.set_defaults(func=init)

    runserver_parser = subcommand_parser.add_parser(
        "runserver", help="run Django server (default)"
    )
    runserver_parser.set_defaults(func=runserver)
    args = parser.parse_args(sys.argv[1:] if len(sys.argv[1:]) > 0 else ["runserver"])
    
    args.func(args)


if __name__ == "__main__":
    main()
import glob
from pathlib import Path
import json
import csv
import os
import logging

from django.db import connections
from django.conf import settings
from django.test import TestCase

from psycopg2 import sql

from visione.lib.compute import compute

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class VisioneTestCase(TestCase):
    def setUp(self):
        csv_glob = os.path.join(settings.BASE_DIR, "..", "..", "..", "init", "*.csv")
        csv_files = sorted(glob.glob(csv_glob))
        for csv_file in csv_files:
            db_table = os.path.basename(csv_file).split(".")[1]
            with open(csv_file) as file:
                reader = csv.reader(file, delimiter=",")
                headers = next(reader)
                with connections["default"].cursor() as cursor:
                    file.seek(0)
                    cursor.copy_expert(
                        sql.SQL(
                            "COPY public.{}({}) FROM STDIN DELIMITER ',' CSV HEADER".format(
                                db_table, ",".join(headers)
                            )
                        ),
                        file,
                    )

    def test_json(self):
        json_glob = os.path.join(settings.BASE_DIR, "tests", "*.json")
        json_files = sorted(glob.glob(json_glob))
        for json_file in json_files:
            json_file_name = os.path.basename(json_file)
            with self.subTest(json_file_name):
                logger.info("Testing with inputs/outputs from: %s", json_file_name)

                data = json.loads(Path(json_file).read_text())
                inputs, expected_outputs = data["inputs"], data["outputs"]
                outputs = compute(inputs)
                logger.info("Outputs: %s", json.dumps(outputs, indent=2))

                self.assertEqual(len(expected_outputs), len(outputs))
                for output, expected_output in zip(outputs, expected_outputs):
                    self.assertEqual(expected_output["plot_id"], output["plot_id"])
                    self.assertEqual(expected_output["order_id"], output["order_id"])
                    for attribute in [
                        "n_requirement",
                        "p2o5_requirement",
                        "k2o_requirement",
                        "n_recommendation",
                        "p2o5_recommendation",
                        "k2o_recommendation",
                    ]:
                        if attribute in expected_output:
                            if expected_output[attribute] is None:
                                self.assertIsNone(output[attribute], msg=attribute)
                            else:
                                self.assertIsNotNone(output[attribute], msg=attribute)
                                self.assertAlmostEqual(
                                    expected_output[attribute],
                                    output[attribute],
                                    delta=1,
                                    msg=attribute,
                                )

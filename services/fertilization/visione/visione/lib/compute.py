from constance import config

from visione.models import (
    Profile,
    OrganicFertilizer,
    OrganicFertilizerPeriodAndMethodOfDistribution,
)
from visione.lib.objects import (
    Fertilization,
    ProductionPhase,
    Rotation,
    Texture,
    SoilSample,
    ManureDistribution,
    Strategy,
    Plan,
)
from visione.lib.recommendation import Recommendation


def compute(inputs):

    # Example inputs as received from Hasura
    # [
    #      {
    #         "current_rotations":[
    #            {
    #               "crop_yield":1,
    #               "is_green_manure_cover_crop":false,
    #               "number_of_cycles":1,
    #               "order_id":1,
    #               "fertilizations":[
    #                  {
    #                     "id":2,
    #                     "dose":1.5,
    #                     "period_and_method_of_distribution_id":3,
    #                     "custom_concentration":{
    #                        "n":0.39,
    #                        "p2o5":0.08,
    #                        "k2o":0.13
    #                     }
    #                  }
    #               ],
    #               "production_phase_id":null,
    #               "profile_id":4,
    #               "humidity": 50,
    #               "residues_are_buried":true
    #            },
    #            {
    #               "crop_yield":2,
    #               "is_green_manure_cover_crop":null,
    #               "number_of_cycles":3,
    #               "order_id":2,
    #               "fertilizations":[
    #                  {
    #                     "organic_fertilizer_id":4,
    #                     "dose":2.5,
    #                     "period_and_method_of_distribution_id":4,
    #                     "custom_concentration": {
    #                        "n":0.48,
    #                        "p2o5":0.08,
    #                        "k2o":0.4
    #                     }
    #                  }
    #               ],
    #               "production_phase_id":null,
    #               "profile_id":9,
    #               "residues_are_buried":false
    #            }
    #         ],
    #         "plot_id":1233,
    #         "previous_rotation": {
    #    "profile_id":1,
    #    "crop_yield":1,
    #    "number_of_cycles":5,
    #    "fertilization": {
    #       "dose":2,
    #       "organic_fertilizer_id":28,
    #       "custom_concentration": {
    #          "n":0.16,
    #          "p2o5":0.05,
    #          "k2o":0.07
    #       }
    #    },
    #    "residues_are_buried":true
    #         },
    #         "soil":{
    #            "texture":"franco_argilloso",
    #            "soil_organic_matter":1.8,
    #            "c_n":8.4,
    #            "p_olsen":23,
    #            "k":34,
    #            "ph":7,
    #            "is_limy":true,
    #            "clay":33,
    #            "sand":33
    #         },
    #         "strategy":{
    #            "n_target":0,
    #            "p2o5_target":0,
    #            "k2o_target":0,
    #            "manure_distribution":"previous_year_yes"
    #         }
    #      }
    #   ]

    outputs = []

    for plot in inputs:

        rotations = []

        if plot.get("previous_rotation", None) is not None:
            plot["previous_rotation"]["order_id"] = 0
            all_rotations_dict = [plot["previous_rotation"]] + plot["current_rotations"]
        else:
            all_rotations_dict = plot["current_rotations"]

        for rotation_dict in all_rotations_dict:

            profile = Profile.objects.get(pk=rotation_dict["profile_id"])

            fertilizations = []

            fertilizations_list = rotation_dict.get("fertilizations", None)
            if fertilizations_list is None:
                fertilizations_list = []

            for fertilization_dict in fertilizations_list:
                organic_fertilizer = OrganicFertilizer.objects.get(
                    pk=fertilization_dict["organic_fertilizer_id"]
                )

                if (
                    fertilization_dict.get("period_and_method_of_distribution_id", None)
                    is not None
                ):
                    period_and_method_of_distribution = (
                        OrganicFertilizerPeriodAndMethodOfDistribution.objects.get(
                            pk=fertilization_dict[
                                "period_and_method_of_distribution_id"
                            ]
                        )
                    )
                else:
                    period_and_method_of_distribution = None

                fertilization = Fertilization(
                    organic_fertilizer=organic_fertilizer,
                    dose=fertilization_dict["dose"],
                    n=organic_fertilizer.n,
                    p2o5=organic_fertilizer.p2o5,
                    k2o=organic_fertilizer.k2o,
                    period_and_method_of_distribution=period_and_method_of_distribution,
                )

                # If we receive custom concentrations then override the concentrations of the fertilization
                custom_concentrations = fertilization_dict.get(
                    "custom_concentrations", {}
                )
                fertilization.n = custom_concentrations.get("n", fertilization.n)
                fertilization.p2o5 = custom_concentrations.get(
                    "p2o5", fertilization.p2o5
                )
                fertilization.k2o = custom_concentrations.get("k2o", fertilization.k2o)

                fertilizations.append(fertilization)

            production_phase = (
                getattr(ProductionPhase, rotation_dict["production_phase"])
                if rotation_dict.get("production_phase", None) is not None
                else None
            )

            if "humidity" in rotation_dict and rotation_dict["humidity"] is not None:
                humidity = rotation_dict["humidity"]
            else:
                humidity = profile.humidity

            if (
                "number_of_cycles" in rotation_dict
                and rotation_dict["number_of_cycles"] is not None
            ):
                number_of_cycles = rotation_dict["number_of_cycles"]
            else:
                number_of_cycles = 1

            if (
                "crop_yield" in rotation_dict
                and rotation_dict["crop_yield"] is not None
            ):
                crop_yield = rotation_dict["crop_yield"]
            else:
                crop_yield = 0

            if (
                "residues_are_buried" in rotation_dict
                and rotation_dict["residues_are_buried"] is not None
            ):
                residues_are_buried = rotation_dict["residues_are_buried"]
            else:
                residues_are_buried = False

            if (
                "is_green_manure_cover_crop" in rotation_dict
                and rotation_dict["is_green_manure_cover_crop"] is not None
            ):
                is_green_manure_cover_crop = rotation_dict["is_green_manure_cover_crop"]
            else:
                is_green_manure_cover_crop = False

            rotation = Rotation(
                order_id=rotation_dict["order_id"],
                profile=profile,
                number_of_cycles=number_of_cycles,
                crop_yield=crop_yield,
                humidity=humidity,
                residues_are_buried=residues_are_buried,
                fertilizations=fertilizations,
                is_green_manure_cover_crop=is_green_manure_cover_crop,
                production_phase=production_phase,
                previous_rotation=None,
            )

            rotations.append(rotation)

        # Assign a reference to the previous rotation, in each rotation except the first
        for idx_rotation, rotation in enumerate(rotations):
            if idx_rotation > 0:
                rotation.previous_rotation = rotations[idx_rotation - 1]

        # Extract texture
        if (
            plot.get("soil", {}).get("clay", None) is not None
            and plot.get("soil", {}).get("sand", None) is not None
        ):
            # If clay and sand are provided then dereive texture from clay+sand
            texture = Texture.texture_from_clay_sand(
                plot["soil"]["clay"], plot["soil"]["sand"]
            )
        elif plot.get("soil", {}).get("texture", None) is not None:
            # Else expect a USDA 12-class texture and map to the Visione 3 classes
            texture = Texture.texture_from_usda(plot["soil"]["texture"])
        else:
            texture = None

        # Extract soil data
        soil_sample = SoilSample(
            clay=plot.get("soil", {}).get("clay", None),
            sand=plot.get("soil", {}).get("sand", None),
            soil_organic_matter=plot.get("soil", {}).get("soil_organic_matter", None),
            c_n=plot.get("soil", {}).get("c_n", None),
            p_olsen=plot.get("soil", {}).get("p_olsen", None),
            k=plot.get("soil", {}).get("k", None),
            ph=plot.get("soil", {}).get("ph", None),
        )

        # Extract strategy
        manure_distribution = plot.get("strategy", {}).get("manure_distribution", None)
        if manure_distribution is not None:
            manure_distribution = getattr(ManureDistribution, manure_distribution)
        strategy = Strategy(
            manure_distribution=manure_distribution,
            n_target=plot.get("strategy", {}).get("n_target", config.N_TARGET),
            p2o5_target=plot.get("strategy", {}).get("p2o5_target", config.P2O5_TARGET),
            k2o_target=plot.get("strategy", {}).get("k2o_target", config.K20_TARGET),
        )

        # Trim out the first rotation (from the previous campaign) if it has been
        # added in the first place
        if plot.get("previous_rotation", None) is not None:
            rotations = rotations[1:]

        # Build plan
        plan = Plan(
            rotations=rotations,
            texture=texture,
            soil_sample=soil_sample,
            strategy=strategy,
        )

        # Compute NPK recommendation and advices
        recommendations = Recommendation(plan=plan).npk_recommendation()

        # Format for output
        for r in recommendations:
            outputs.append({"plot_id": plot["plot_id"], **r})

    return outputs

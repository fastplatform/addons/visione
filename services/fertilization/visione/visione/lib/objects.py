from django.utils.translation import ugettext_lazy as _

class USDATexture:
    ARGILLOSO = "argilloso"
    ARGILLOSO_LIMOSO = "argilloso_limoso"
    ARGILLOSO_SABBIOSO = "argilloso_sabbioso"
    FRANCO_SABBIOSO = "franco_sabbioso"
    SABBIOSO = "sabbioso"
    SABBIOSO_FRANCO = "sabbioso_franco"
    FRANCO = "franco"
    FRANCO_ARGILLOSO = "franco_argilloso"
    FRANCO_LIMOSO = "franco_limoso"
    FRANCO_LIMOSO_ARGILLOSO = "franco_limoso_argilloso"
    FRANCO_SABBIOSO_ARGILLOSO = "franco_sabbioso_argilloso"
    LIMOSO = "limoso"


class Texture:
    """Soil texture based on 3 classes"""

    CLAYEY = "CLAYEY"
    SANDY = "SANDY"
    LOAM = "LOAM"

    @staticmethod
    def texture_from_clay_sand(clay, sand):
        "Returns the soil texture based on % sand, clay"
        if clay > 28 and sand < 20:
            return Texture.CLAYEY
        if clay > 40 and sand > 20:
            return Texture.CLAYEY
        if clay > 36 and sand > 45:
            return Texture.CLAYEY
        if sand > 43 and sand < 52 and clay < 7:
            return Texture.SANDY
        if sand > 52 and clay < 20:
            return Texture.SANDY
        else:
            return Texture.LOAM

    @staticmethod
    def texture_from_usda(usda_texture):
        "Returns the soil texture based on 12 classes USDA"
        if usda_texture in [
            USDATexture.ARGILLOSO,
            USDATexture.ARGILLOSO_LIMOSO,
            USDATexture.ARGILLOSO_SABBIOSO,
        ]:
            return Texture.CLAYEY
        if usda_texture in [
            USDATexture.FRANCO_SABBIOSO,
            USDATexture.SABBIOSO,
            USDATexture.SABBIOSO_FRANCO,
        ]:
            return Texture.SANDY
        if usda_texture in [
            USDATexture.FRANCO,
            USDATexture.FRANCO_ARGILLOSO,
            USDATexture.FRANCO_LIMOSO,
            USDATexture.FRANCO_LIMOSO_ARGILLOSO,
            USDATexture.LIMOSO,
        ]:
            return Texture.LOAM
        return -1


class ManureDistribution:
    annual = "annual"
    previous_year_yes = "previous_year_yes"
    previous_year_no = "previous_year_no"


class ProductionPhase:
    establishment = "establishment"
    year_1 = "year_1"
    year_2 = "year_2"
    production = "production"


class ProfileGroup:
    arboree = "arboree"
    orticole = "orticole"
    erbacee = "erbacee"
    frutti_minori = "frutti minori"


class SoilSample:
    """Results of soil sample analysis in a laboratory
    c_n: ratio C/N
    soil_organic_matter: soil organic matter
    is_limy: % of total limestone >10
    ph: ph in soil
    p_olsen: (ppm)
    k: (ppm)
    clay: % clay in soil
    sand: % sand in soil
    """

    def __init__(
        self,
        soil_organic_matter,
        c_n,
        p_olsen,
        k,
        ph,
        clay,
        sand=None,
        is_limy=False,
    ):
        if soil_organic_matter is not None:
            assert isinstance(soil_organic_matter, (float, int))
            assert 0 <= soil_organic_matter <= 100
        if c_n is not None:
            assert isinstance(c_n, (float, int))
            assert c_n >= 0
        if p_olsen is not None:
            assert isinstance(p_olsen, (float, int))
            assert p_olsen >= 0
        if k is not None:
            assert isinstance(k, (float, int))
            assert k >= 0
        if ph is not None:
            assert isinstance(ph, (float, int))
            assert 0 <= ph <= 14
        if clay is not None:
            assert isinstance(clay, (float, int))
            assert 0 <= clay <= 100
        if sand is not None:
            assert isinstance(sand, (float, int))
            assert 0 <= sand <= 100
        if is_limy is not None:
            assert isinstance(is_limy, bool)

        self.soil_organic_matter = soil_organic_matter
        self.c_n = c_n
        self.p_olsen = p_olsen
        self.k = k
        self.ph = ph
        self.clay = clay
        self.sand = sand
        self.is_limy = is_limy

    @staticmethod
    def calculate_coefficient_mineralization(c_n, texture):
        "Returns mineralization coefficient"
        if 9 <= c_n <= 12:
            if texture == Texture.SANDY:
                return 36
            elif texture == Texture.LOAM:
                return 24
            elif texture == Texture.CLAYEY:
                return 14
        elif c_n < 9:
            if texture == Texture.SANDY:
                return 42
            elif texture == Texture.LOAM:
                return 26
            elif texture == Texture.CLAYEY:
                return 18
        else:
            if texture == Texture.SANDY:
                return 24
            elif texture == Texture.LOAM:
                return 20
            elif texture == Texture.CLAYEY:
                return 6
        raise ValueError()

    # @staticmethod
    # def limestone_assessment(limestone: float):
    #     "Returns limestone assessment"
    #     if limestone * 10 < 10:
    #         return "no limy"
    #     elif limestone * 10 >= 10 and limestone * 10 < 100:
    #         return "slight limy"
    #     elif limestone * 10 >= 100 and limestone * 10 < 250:
    #         return "averagely limy"
    #     elif limestone * 10 >= 250 and limestone * 10 < 500:
    #         return "limy"
    #     else:
    #         return "very limy"

    @staticmethod
    def calculate_coefficient_p(p_olsen, texture):
        "Returns coefficient p"

        POOR_LOW_SUPPLY = 1
        MEDIUM_SUPPLY = 0.75
        HIGH_SUPPLY = 0.5
        VERY_HIGH_SUPPLY = 0

        if texture == Texture.SANDY and p_olsen < 5:
            return POOR_LOW_SUPPLY
        elif texture == Texture.SANDY and p_olsen >= 5 and p_olsen < 10:
            return POOR_LOW_SUPPLY
        elif texture == Texture.SANDY and p_olsen >= 10 and p_olsen < 15:
            return MEDIUM_SUPPLY
        elif texture == Texture.SANDY and p_olsen >= 15 and p_olsen < 30:
            return HIGH_SUPPLY
        elif texture == Texture.SANDY and p_olsen >= 30:
            return VERY_HIGH_SUPPLY
        elif texture == Texture.LOAM and p_olsen < 8:
            return POOR_LOW_SUPPLY
        elif texture == Texture.LOAM and p_olsen >= 8 and p_olsen < 11:
            return POOR_LOW_SUPPLY
        elif texture == Texture.LOAM and p_olsen >= 11 and p_olsen < 15:
            return MEDIUM_SUPPLY
        elif texture == Texture.LOAM and p_olsen >= 15 and p_olsen < 25:
            return HIGH_SUPPLY
        elif texture == Texture.LOAM and p_olsen >= 25:
            return VERY_HIGH_SUPPLY
        elif texture == Texture.CLAYEY and p_olsen < 5:
            return POOR_LOW_SUPPLY
        elif texture == Texture.CLAYEY and p_olsen >= 5 and p_olsen < 9:
            return POOR_LOW_SUPPLY
        elif texture == Texture.CLAYEY and p_olsen >= 9 and p_olsen < 11:
            return MEDIUM_SUPPLY
        elif texture == Texture.CLAYEY and p_olsen >= 11 and p_olsen < 20:
            return HIGH_SUPPLY
        else:
            return VERY_HIGH_SUPPLY

    @staticmethod
    def calculate_coefficient_k(k, texture):
        "Returns coefficient p"

        POOR_SUPPLY = 1
        LOW_SUPPLY = 0.75
        MEDIUM_SUPPLY = 0.5
        HIGH_SUPPLY = 0

        if texture == Texture.SANDY and k < 40:
            return POOR_SUPPLY
        elif texture == Texture.SANDY and k >= 40 and k < 80:
            return LOW_SUPPLY
        elif texture == Texture.SANDY and k >= 80 and k < 120:
            return MEDIUM_SUPPLY
        elif texture == Texture.SANDY and k >= 120:
            return HIGH_SUPPLY
        elif texture == Texture.LOAM and k < 60:
            return POOR_SUPPLY
        elif texture == Texture.LOAM and k >= 60 and k < 100:
            return LOW_SUPPLY
        elif texture == Texture.LOAM and k >= 100 and k < 150:
            return MEDIUM_SUPPLY
        elif texture == Texture.LOAM and k >= 150:
            return HIGH_SUPPLY
        elif texture == Texture.CLAYEY and k < 80:
            return POOR_SUPPLY
        elif texture == Texture.CLAYEY and k >= 80 and k < 120:
            return LOW_SUPPLY
        elif texture == Texture.CLAYEY and k >= 120 and k < 180:
            return MEDIUM_SUPPLY
        else:
            return HIGH_SUPPLY

    @staticmethod
    def leaching_k(clay):
        "Return leaching k (kg)"
        if clay < 5:
            return 60
        elif clay < 15:
            return 30
        elif clay < 25:
            return 20
        else:
            return 10


class Strategy:
    """
    n_target, p_target, k_target : NPK target concentation
    manure_distribution: ["annual", "previous year, yes", "previous year, no"]
    """

    def __init__(self, n_target, p2o5_target, k2o_target, manure_distribution):
        assert isinstance(n_target, (float, int))
        assert isinstance(p2o5_target, (float, int))
        assert isinstance(k2o_target, (float, int))
        assert manure_distribution is None or hasattr(ManureDistribution, manure_distribution)

        self.n_target = n_target
        self.p2o5_target = p2o5_target
        self.k2o_target = k2o_target
        self.manure_distribution = manure_distribution


class Fertilization:
    """Define the process of fertilization

    fertilizer_name
    period_and_method_of_distribution
    dose: (t to m3 ha-1)
    fertilizer_name
    n:  N (kg t-1 o m-3)
    p: P2O5 (kg t-1 o m-3)
    k: K2O (kg t-1 o m-3)
    coef_efficiency: {"period_and_method_of_distribution": coef_efficiency of N in (%)}
    """

    def __init__(
        self,
        organic_fertilizer,
        dose,
        n,
        p2o5,
        k2o,
        period_and_method_of_distribution,
    ):
        from visione.models import (
            OrganicFertilizer,
            OrganicFertilizerPeriodAndMethodOfDistribution,
        )

        assert isinstance(organic_fertilizer, OrganicFertilizer)
        assert isinstance(dose, (float, int))
        assert dose >= 0
        assert isinstance(n, (float, int))
        assert 0 <= n <= 100
        assert isinstance(p2o5, (float, int))
        assert 0 <= p2o5 <= 100
        assert isinstance(k2o, (float, int))
        assert 0 <= k2o <= 100
        assert isinstance(
            period_and_method_of_distribution,
            OrganicFertilizerPeriodAndMethodOfDistribution,
        ) or period_and_method_of_distribution is None

        self.organic_fertilizer = organic_fertilizer
        self.dose = dose
        self.n = n
        self.p2o5 = p2o5
        self.k2o = k2o
        self.period_and_method_of_distribution = period_and_method_of_distribution


class Rotation:
    """Contains attributes of a single crop rotation
    order_id: order of crop in rotation, first crop start at index 1
    crop_name: name of crop
    residues_are_buried:  True/False
    is_green_manure_cover_crop: green manure/cover crop
    number_of_cycles: number of cycle per crop
    crop_yield: (t ha-1) yield as fresh matter
    production_phase: ["establishment","year_1","year_2","production", None]
    organic_fertilizers: list of fertilizations [Fertilizations]
    """

    def __init__(
        self,
        order_id,
        profile,
        number_of_cycles,
        crop_yield,
        humidity,
        residues_are_buried,
        fertilizations,
        is_green_manure_cover_crop,
        production_phase,
        previous_rotation,
    ):
        from visione.models import Profile

        assert isinstance(order_id, int)
        assert isinstance(profile, Profile)
        assert isinstance(number_of_cycles, int)
        assert isinstance(crop_yield, (float, int))
        assert crop_yield >= 0
        assert isinstance(humidity, (float, int))
        assert 0 <= humidity <= 100
        assert isinstance(residues_are_buried, bool)
        assert isinstance(fertilizations, list)
        assert all([isinstance(f, Fertilization) for f in fertilizations])
        assert isinstance(is_green_manure_cover_crop, bool)
        assert production_phase is None or hasattr(ProductionPhase, production_phase)
        assert previous_rotation is None or isinstance(previous_rotation, Rotation)

        self.profile = profile
        self.number_of_cycles = number_of_cycles  # Only for orticole
        self.crop_yield = crop_yield  # Only for frutti minori, erbacee, orticole
        self.humidity = humidity
        self.residues_are_buried = residues_are_buried
        self.fertilizations = fertilizations
        self.order_id = order_id
        self.previous_rotation = previous_rotation
        self.is_green_manure_cover_crop = is_green_manure_cover_crop
        self.production_phase = production_phase  # Only for arboree

class Plan:
    """Contains all the rotations of a fertilization plan """

    def __init__(
        self,
        rotations,
        texture,
        soil_sample,
        strategy,
    ):
        assert isinstance(rotations, list)
        assert all([isinstance(r, Rotation) for r in rotations])
        if texture is not None:
            assert hasattr(Texture, texture)
        if soil_sample is not None:
            assert isinstance(soil_sample, SoilSample)
        assert isinstance(strategy, Strategy)

        self.rotations = rotations
        self.texture = texture
        self.soil_sample = soil_sample
        self.strategy = strategy

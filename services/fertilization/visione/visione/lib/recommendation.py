import logging

from visione.lib.objects import (
    ManureDistribution,
    ProfileGroup,
    ProductionPhase,
    SoilSample,
)

logger = logging.getLogger(__name__)


class Recommendation:
    """N, P and K recommendations"""

    def __init__(self, plan):
        self.plan = plan

    def crop_yield_dry_matter(self, rotation):
        """Crop yield as dry matter (t ha-1)"""

        dry_matter = (
            rotation.crop_yield
            * ((100 - rotation.humidity) / 100)
            * rotation.number_of_cycles
        )
        logger.debug("crop_yield_dry_matter: %s", dry_matter)
        return dry_matter

    def npk_take_away(self, rotation):
        """(N,P,K) take-away main product (kg ha-1)"""

        if rotation.is_green_manure_cover_crop:
            take_away = [0, 0, 0]
        else:
            crop_yield = Recommendation.crop_yield_dry_matter(self, rotation)
            take_away = [
                (crop_yield * 1000) * (rotation.profile.n / 100),
                (crop_yield * 1000) * (rotation.profile.p2o5 / 100),
                (crop_yield * 1000) * (rotation.profile.k2o / 100),
            ]
        logger.debug("npk_take_away: %s", take_away)
        return take_away

    def crop_residues_yield_dry_matter(self, rotation):
        """Crop residues yield as dry matter (t ha-1)"""

        dry_matter = Recommendation.crop_yield_dry_matter(self, rotation)
        if not rotation.is_green_manure_cover_crop:
            return (
                dry_matter
                * (100 - rotation.profile.harvest_index)
                / rotation.profile.harvest_index
            )

        logger.debug("crop_residues_yield_dry_matter: %s", dry_matter)
        return dry_matter

    def npk_crop_residues_removal(self, rotation):
        """(N,P,K) crop residues removal (kg ha-1)"""

        crop_residues_yield = Recommendation.crop_residues_yield_dry_matter(
            self, rotation
        )
        removal = [
            (crop_residues_yield * 1000) * (rotation.profile.n_in_residues / 100),
            (crop_residues_yield * 1000) * (rotation.profile.p2o5_in_residues / 100),
            (crop_residues_yield * 1000) * (rotation.profile.k2o_in_residues / 100),
        ]

        logger.debug("npk_crop_residues_removal: %s", removal)
        return removal

    def npk_absorption(self, rotation):
        """(N,P,K) absorption (kg ha-1)"""

        if rotation.is_green_manure_cover_crop:
            absorption = [0, 0, 0]
        else:
            n_take_away, p_take_away, k_take_away = Recommendation.npk_take_away(
                self, rotation
            )

            (
                n_crop_residues_removal,
                p_crop_residues_removal,
                k_crop_residues_removal,
            ) = Recommendation.npk_crop_residues_removal(self, rotation)

            n_absorption = n_take_away + n_crop_residues_removal

            if rotation.profile.coef_reintegration is not None:
                n_absorption = rotation.profile.coef_reintegration * n_absorption
            p_absorption = p_take_away + p_crop_residues_removal
            k_absorption = k_take_away + k_crop_residues_removal
            absorption = [n_absorption, p_absorption, k_absorption]

        logger.debug("npk_absorption: %s", absorption)
        return absorption

    def kt_absorption(self, rotation):
        """Kt absorption (kg ha-1)"""

        absorption = rotation.profile.kt * rotation.number_of_cycles
        logger.debug("kt_absorption: %s", absorption)
        return absorption

    def kt_modulated(self, rotation):
        """Kt modulated (no unit)"""

        sum_kt_absorption = 0
        for r in self.plan.rotations:
            sum_kt_absorption += Recommendation.kt_absorption(self, r)
        if sum_kt_absorption > 100:
            kt = Recommendation.kt_absorption(self, rotation) * 100 / sum_kt_absorption
        else:
            kt = Recommendation.kt_absorption(self, rotation)

        logger.debug("kt_modulated: %s", kt)
        return kt

    def mineralization_soil_organic(self, rotation):
        """Mineralization soil organic (kg N) """

        coeff_mineralization = SoilSample.calculate_coefficient_mineralization(
            self.plan.soil_sample.c_n, self.plan.texture
        )
        mineralization = (
            coeff_mineralization
            * (self.plan.soil_sample.soil_organic_matter / 100)
            * Recommendation.kt_modulated(self, rotation)
        )

        logger.debug("mineralization_soil_organic: %s", mineralization)
        return mineralization

    def previous_crop_residues_yield_dry_matter(self, rotation):
        if (
            rotation.order_id == 2
            and rotation.previous_rotation is not None
            and rotation.previous_rotation.is_green_manure_cover_crop
        ):
            dry_matter = Recommendation.crop_residues_yield_dry_matter(
                self, rotation.previous_rotation
            )
        else:
            dry_matter = (
                (100 - rotation.previous_rotation.profile.harvest_index)
                / rotation.previous_rotation.profile.harvest_index
                * Recommendation.crop_yield_dry_matter(self, rotation.previous_rotation)
            )

        logger.debug("previous_crop_residues_yield_dry_matter: %s", dry_matter)
        return dry_matter

    def n_previous_crop_residues(self, rotation):
        """N of previous crop residues (kg ha-1)"""

        kt_mod = Recommendation.kt_modulated(self, rotation)
        if (
            rotation.order_id == 2
            and rotation.previous_rotation is not None
            and rotation.previous_rotation.is_green_manure_cover_crop
        ):
            n_crop_residues_removal = Recommendation.npk_crop_residues_removal(
                self, rotation.previous_rotation
            )[0]
            residues = n_crop_residues_removal * kt_mod / 100
        else:
            if (
                rotation.previous_rotation is not None
                and rotation.previous_rotation.residues_are_buried
            ):
                if (
                    rotation.previous_rotation.profile.mineralization_crop_residues
                    is not None
                ):
                    residues = (
                        rotation.previous_rotation.profile.mineralization_crop_residues
                        / 100
                    ) * kt_mod
                else:
                    residues = (
                        Recommendation.previous_crop_residues_yield_dry_matter(
                            self, rotation
                        )
                        * (rotation.previous_rotation.profile.n_in_residues / 100)
                        * 10
                        * kt_mod
                    )
            else:
                residues = 0

        logger.debug("n_previous_crop_residues: %s", residues)
        return residues

    def p_previous_crop_residues(self, rotation):
        """P of previous crop residues (kg ha-1)"""

        kt_mod = Recommendation.kt_modulated(self, rotation)
        if (
            rotation.previous_rotation is not None
            and rotation.previous_rotation.residues_are_buried
        ):
            residues = (
                Recommendation.previous_crop_residues_yield_dry_matter(self, rotation)
                * rotation.previous_rotation.profile.p2o5_in_residues
                * 10
                * kt_mod
                / 100
            )
        else:
            residues = 0

        logger.debug("p_previous_crop_residues: %s", residues)
        return residues

    def k_previous_crop_residues(self, rotation):
        """K of previous crop residues (kg ha-1)"""

        kt_mod = Recommendation.kt_modulated(self, rotation)
        if (
            rotation.previous_rotation is not None
            and rotation.previous_rotation.residues_are_buried
        ):
            residues = (
                Recommendation.previous_crop_residues_yield_dry_matter(self, rotation)
                * rotation.previous_rotation.profile.k2o_in_residues
                * 10
                * kt_mod
                / 100
            )
        else:
            residues = 0

        logger.debug("k_previous_crop_residues: %s", residues)
        return residues

    def manure_distribution_effect(self, rotation):
        """Mf effect of solid manure distribution (kg N)  """

        if (
            self.plan.strategy.manure_distribution
            == ManureDistribution.previous_year_yes
        ):
            if (
                self.plan.rotations[0].previous_rotation is not None
                and self.plan.rotations[0].previous_rotation.fertilizations is not None
            ):
                solid_manure_distribution_effect = (
                    self.plan.rotations[0].previous_rotation.fertilizations[0].dose
                    * self.plan.rotations[0]
                    .previous_rotation.fertilizations[0]
                    .organic_fertilizer.n
                )
                effect = (
                    solid_manure_distribution_effect
                    * 0.3
                    * Recommendation.kt_modulated(self, rotation)
                    / 100
                )
            else:
                effect = 0
        else:
            effect = 0

        logger.debug("manure_distribution_effect: %s", effect)
        return effect

    def deposition_atmospheric(self, rotation):
        """Da: Atmospheric N deposition (kg N) """

        deposition = 20 * Recommendation.kt_modulated(self, rotation) / 100

        logger.debug("deposition_atmospheric: %s", deposition)
        return deposition

    def bfx_adjusted(self, rotation):
        """Adjusted bfx """

        n_for_bfx = Recommendation.npk_absorption(self, rotation)[0] - (
            Recommendation.mineralization_soil_organic(self, rotation)
            + Recommendation.n_previous_crop_residues(self, rotation)
            + Recommendation.manure_distribution_effect(self, rotation)
            + Recommendation.deposition_atmospheric(self, rotation)
            + rotation.profile.n_fixation
        )

        if n_for_bfx < 0:
            bfx = max(rotation.profile.n_fixation - abs(n_for_bfx), 0)
        else:
            bfx = rotation.profile.n_fixation

        logger.debug("bfx_adjusted: %s", bfx)
        return bfx

    def n_requirements(self, rotation):
        requirements = max(
            Recommendation.npk_absorption(self, rotation)[0]
            - (
                Recommendation.mineralization_soil_organic(self, rotation)
                + Recommendation.n_previous_crop_residues(self, rotation)
                + Recommendation.manure_distribution_effect(self, rotation)
                + Recommendation.deposition_atmospheric(self, rotation)
                + Recommendation.bfx_adjusted(self, rotation)
            ),
            0,
        )
        if (
            rotation.profile.profile_option is not None
            and "vigoria" in rotation.profile.profile_option
        ):
            requirements *= rotation.profile.coef_reintegration

        logger.debug("n_requirements: %s", requirements)
        return requirements

    def p_requirements(self, rotation):

        if (
            "Mais" in rotation.profile.profile_name
            and SoilSample.calculate_coefficient_p(
                self.plan.soil_sample.p_olsen, self.plan.texture
            )
            == 0
            and (
                self.plan.soil_sample.ph < 6.1
                or self.plan.soil_sample.ph > 7.9
                or self.plan.soil_sample.is_limy
            )
        ):
            requirements = 40
        elif SoilSample.calculate_coefficient_p(
            self.plan.soil_sample.p_olsen, self.plan.texture
        ) == 0 and (
            self.plan.soil_sample.ph < 6.1
            or self.plan.soil_sample.ph > 7.9
            or self.plan.soil_sample.is_limy
        ):
            requirements = 20
        else:
            requirements = SoilSample.calculate_coefficient_p(
                self.plan.soil_sample.p_olsen, self.plan.texture
            ) * (
                Recommendation.npk_absorption(self, rotation)[1]
                - Recommendation.p_previous_crop_residues(self, rotation)
            )

        logger.debug("p_requirements: %s", requirements)
        return requirements

    def k_requirements(self, rotation):
        requirements = SoilSample.calculate_coefficient_k(
            self.plan.soil_sample.k, self.plan.texture
        ) * (
            Recommendation.npk_absorption(self, rotation)[2]
            - Recommendation.k_previous_crop_residues(self, rotation)
            + SoilSample.leaching_k(self.plan.soil_sample.clay)
        )
        if requirements <= 0:
            requirements = 50

        logger.debug("k_requirements: %s", requirements)
        return requirements

    def npk_recommendation(self):
        """ (N,P,K) Recommendation """

        recommendations = []

        for rotation in self.plan.rotations:
            recommendation = {"order_id": rotation.order_id}

            n_organic_fertilizer = 0
            p2o5_organic_fertilizer = 0
            k2o_organic_fertilizer = 0

            if rotation.fertilizations:
                for fertilization in rotation.fertilizations:
                    coef_efficency = (
                        fertilization.period_and_method_of_distribution.coef_efficiency
                    )
                    logger.debug(
                        "cief_efficiency: %s %s",
                        fertilization.organic_fertilizer.name,
                        coef_efficency,
                    )

                    if (
                        (
                            "letame" in fertilization.organic_fertilizer.name.lower()
                            or "compost"
                            in fertilization.organic_fertilizer.name.lower()
                        )
                        and self.plan.strategy.manure_distribution
                        == ManureDistribution.annual
                    ):
                        coef_efficency += 30

                    n_organic_fertilizer += (
                        # Percentage of N
                        (fertilization.n / 100)
                        # Tons to kgs
                        * (fertilization.dose * 1000)
                        # Percentage of efficiency
                        * (coef_efficency / 100)
                    )

                    p2o5_organic_fertilizer += (
                        # Percentage of P2O5
                        (fertilization.p2o5 / 100)
                        # Tons to kgs
                        * (fertilization.dose * 1000)
                    )
                    k2o_organic_fertilizer += (
                        # Percentage of P2O5
                        (fertilization.k2o / 100)
                        # Tons to kgs
                        * (fertilization.dose * 1000)
                    )

            # Recommendation N
            if rotation.profile.profile_group == ProfileGroup.arboree:
                n_requirements = None
                if rotation.production_phase == ProductionPhase.establishment:
                    n_recommendation = 0
                elif (
                    rotation.production_phase == ProductionPhase.year_1
                    and n_organic_fertilizer < rotation.profile.n_standard_dose * 0.4
                ):
                    n_recommendation = (
                        rotation.profile.n_standard_dose * 0.4 - n_organic_fertilizer
                    )
                elif (
                    rotation.production_phase == ProductionPhase.year_2
                    and n_organic_fertilizer < rotation.profile.n_standard_dose * 0.5
                ):
                    n_recommendation = (
                        rotation.profile.n_standard_dose * 0.5 - n_organic_fertilizer
                    )
                elif (
                    rotation.production_phase == ProductionPhase.production
                    and n_organic_fertilizer < rotation.profile.n_standard_dose
                ):
                    n_recommendation = (
                        rotation.profile.n_standard_dose - n_organic_fertilizer
                    )
                else:
                    n_recommendation = 0
            else:
                n_requirements = Recommendation.n_requirements(self, rotation)
                n_recommendation = max(n_requirements - n_organic_fertilizer, 0)

            # Recommendation P2O5
            if rotation.profile.profile_group == ProfileGroup.arboree:
                p2o5_requirements = None
                if rotation.production_phase == ProductionPhase.establishment:
                    p2o5_recommendation = 0
                elif (
                    rotation.production_phase == ProductionPhase.year_1
                    and p2o5_organic_fertilizer
                    < rotation.profile.p2o5_standard_dose * 0.3
                ):
                    p2o5_recommendation = (
                        rotation.profile.p2o5_standard_dose * 0.3
                        - p2o5_organic_fertilizer
                    )
                elif (
                    rotation.production_phase == ProductionPhase.year_2
                    and p2o5_organic_fertilizer
                    < rotation.profile.p2o5_standard_dose * 0.5
                ):
                    p2o5_recommendation = (
                        rotation.profile.p2o5_standard_dose * 0.5
                        - p2o5_organic_fertilizer
                    )
                elif (
                    rotation.production_phase == ProductionPhase.production
                    and p2o5_organic_fertilizer < rotation.profile.p2o5_standard_dose
                ):
                    p2o5_recommendation = (
                        rotation.profile.p2o5_standard_dose - p2o5_organic_fertilizer
                    )
                else:
                    p2o5_recommendation = 0
            else:
                p2o5_requirements = Recommendation.p_requirements(self, rotation)
                p2o5_recommendation = max(
                    p2o5_requirements - p2o5_organic_fertilizer, 0
                )

            # Recommendation K2O
            if rotation.profile.profile_group == ProfileGroup.arboree:
                k2o_requirements = None
                if rotation.production_phase == ProductionPhase.establishment:
                    k2o_recommendation = 0
                elif (
                    rotation.production_phase == ProductionPhase.year_1
                    and k2o_organic_fertilizer
                    < rotation.profile.k2o_standard_dose * 0.2
                ):
                    k2o_recommendation = (
                        rotation.profile.k2o_standard_dose * 0.2
                        - k2o_organic_fertilizer
                    )
                elif (
                    rotation.production_phase == ProductionPhase.year_2
                    and k2o_organic_fertilizer
                    < rotation.profile.k2o_standard_dose * 0.4
                ):
                    k2o_recommendation = (
                        rotation.profile.k2o_standard_dose * 0.4
                        - k2o_organic_fertilizer
                    )
                elif (
                    rotation.production_phase == ProductionPhase.production
                    and k2o_organic_fertilizer < rotation.profile.k2o_standard_dose
                ):
                    k2o_recommendation = (
                        rotation.profile.k2o_standard_dose - k2o_organic_fertilizer
                    )
                else:
                    k2o_recommendation = 0
            else:
                k2o_requirements = Recommendation.k_requirements(self, rotation)
                k2o_recommendation = max(k2o_requirements - k2o_organic_fertilizer, 0)

            # Standard doses
            if rotation.profile.profile_group == ProfileGroup.arboree:
                n_standard_dose = rotation.profile.n_standard_dose
                p2o5_standard_dose = rotation.profile.p2o5_standard_dose
                k2o_standard_dose = rotation.profile.k2o_standard_dose
            else:
                n_standard_dose = None
                p2o5_standard_dose = None
                k2o_standard_dose = None

            n_recommendation += self.plan.strategy.n_target
            p2o5_recommendation += self.plan.strategy.p2o5_target
            k2o_recommendation += self.plan.strategy.k2o_target

            recommendation["n_requirement"] = n_requirements
            recommendation["p2o5_requirement"] = p2o5_requirements
            recommendation["k2o_requirement"] = k2o_requirements

            recommendation["n_organic_fertilizer"] = n_organic_fertilizer
            recommendation["p2o5_organic_fertilizer"] = p2o5_organic_fertilizer
            recommendation["k2o_organic_fertilizer"] = k2o_organic_fertilizer

            recommendation["n_standard_dose"] = n_standard_dose
            recommendation["p2o5_standard_dose"] = p2o5_standard_dose
            recommendation["k2o_standard_dose"] = k2o_standard_dose

            recommendation["n_recommendation"] = max(n_recommendation, 0)
            recommendation["p2o5_recommendation"] = max(p2o5_recommendation, 0)
            recommendation["k2o_recommendation"] = max(k2o_recommendation, 0)

            recommendation["proposed_strategies"] = list(
                rotation.profile.profile_strategies.values_list(
                    "proposed_strategy", flat=True
                )
            )

            recommendations.append(recommendation)

        recommendations = sorted(recommendations, key=lambda r: r["order_id"])

        return recommendations

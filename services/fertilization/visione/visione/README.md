# Visione

### Visione fertilization advice algorithm (Piemonte)

* Generating distribution archives: python setup bdist_wheel
* Install package: pip install visione 


    ```Input example of dict format:
    {
        "rotation":{
            "previous_rotation":{
                "profile_name":"Mais silo 2° epoca",
                "residues_are_buried" : False,
                "crop_yield" : 55,
                "n_cycles": 1,
                "organic_fertilizers":
                        {
                            "fertilizer_name":"Letame bovino",
                            "dose":18
                        }
                        
                },
            "actual_rotation":[
                {
                    "order_id":1,
                    "profile_name":"Loiessa erbaio",
                    "residues_are_buried": False ,
                    "is_green_manure_cover_crop":False,
                    "n_cycles":1,
                    "crop_yield":10,
                    "production_phase": None,
                    "organic_fertilizers":[
                        {
                            "fertilizer_name":"Liquami di bovini ta quali",
                            "period_and_method_of_distribution":"p-e1",
                            "dose":18
                        },
                        {
                            "fertilizer_name": "Letame bovino",
                            "period_and_method_of_distribution":"p-e2",
                            "dose":37
                        }
                        ]
                },
                {
                    "order_id":2,
                    "profile_name":"Mais silo 2° epoca",
                    "residues_are_buried": False,
                    "n_cycles":1,
                    "crop_yield":55,
                    "organic_fertilizers":[
                        {
                            "fertilizer_name":"Liquami di bovini ta quali",
                            "period_and_method_of_distribution":"p-e1",
                            "dose":18
                        },
                        {
                            "fertilizer_name": "Letame bovino",
                            "period_and_method_of_distribution":"p-e2",
                            "dose":37
                        }
                        ]
                }
            ]
        },
        "soil":{
            "texture":"Franco"
        },
        "sample":{
            "soil_organic_matter":2.4252,
            "c_n":8.44,
            "p_olsen":164,
            "k":256,
            "ph":6.5,
            "is_limy":False,
            "clay": 9,
            "sand": None
        },
        "strategy":{
            "manure_distribution": "annual",
            "n_target":0,
            "p_target":0,
            "k_target":0
        },
        "crop_book":[
            {
                "profile_name":"Loiessa erbaio",
                "crop_category": "erbacee",
                "humidity":63,
                "n":1.5,
                "p":0.8,
                "k":2.7,
                "harvest_index":100,
                "n_in_residues":1.5,
                "p_in_residues":0.8,
                "k_in_residues":2.7,
                "kt":33,
                "mineralization_crop_residues":None,
                "n_fixation":0,
                "coef_reintegration":None,
                "standard_doses_n":0,
                "standard_doses_p":0,
                "standard_doses_k":0
            },
            {
                "profile_name":"Mais silo 2° epoca",
                "crop_category": "erbacee",
                "humidity":67,
                "n":1.2,
                "p":0.5,
                "k":1.2,
                "harvest_index":100,
                "n_in_residues":1.2,
                "p_in_residues":0.5,
                "k_in_residues":1.2,
                "kt":67,
                "mineralization_crop_residues":None,
                "n_fixation":0,
                "coef_reintegration":None,
                "standard_doses_n":240,
                "standard_doses_p":85,
                "standard_doses_k":150
            }
        ],
        "fertilizer_book":[
            {
                "fertilizer_name":"Liquami di bovini ta quali",
                "n":4.306474,
                "p":2.3,
                "k":4.9,
                "coef_efficiency": {
                    "p-e1":55,
                    "p-e2":55
                    }
            },
            {
                "fertilizer_name":"Letame bovino",
                "n":3.571903,
                "p":2.1,
                "k":4.5,
                "coef_efficiency":{
                    "p-e1":45,
                    "p-e2":45
                    }
            }
            ]
    }
    ```
from collections import OrderedDict

from django.utils.translation import ugettext_lazy as _

VISIONE_CONFIG = OrderedDict(
    [
        ("N_TARGET", (0, _("Target nitrogen surplus"), int)),
        ("P2O5_TARGET", (0, _("Target phosphorus (P2O5) surplus"), int)),
        ("K20_TARGET", (0, _("Target potassium (K20) surplus"), int)),
    ]
)
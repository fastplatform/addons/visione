import json
import os
import requests
import base64

from django.conf import settings
from django.http import HttpResponseForbidden
from django.http.response import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.templatetags.static import static

from visione.lib.compute import compute

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))

@csrf_exempt
@require_http_methods(["POST"])
def handler_hasura_action_npk(request):
    # We receive a JSON with 2 roots:
    # - session_variables: the request headers forwarded by Hasura
    # - input: a JSON representation of the input
    body = json.loads(request.body)

    # If no prooper role is received from Hasura, reject
    if body["session_variables"].get("x-hasura-role", None) not in [
        "farmer",
        "service",
        "admin",
        "add_on",
    ]:
        return HttpResponseForbidden()

    inputs = body["input"]["inputs"]
    outputs = compute(inputs)
    return JsonResponse(outputs, safe=False)


@csrf_exempt
@require_http_methods(['POST'])
def action_handler_generate_results_pdf(request):
    try:
        inputs = json.loads(request.body)
        inputs = inputs['input']['inputs']
        # read the results html template
        with open(CURRENT_DIR + '/templates/pdf/base.html', 'r') as f:
            template = f.read()
        inputs['algorithm_name'] = "Visione"
        try:
            # get algo logo as static image
            algo_logo_static_url = request.build_absolute_uri(static('logo_visione.png'))
            res_algo = requests.get(algo_logo_static_url)
            inputs['algorithm_logo_base64'] = 'data:image/png;base64,' + base64.b64encode(res_algo.content).decode()
        except Exception as e:
            print('FAILED TO GET ALGORITHM LOGO', str(e))
        # generate PDF
        res = requests.post(settings.PDF_GENERATOR_ENDPOINT, json={
            "template": template,
            "data": inputs 
        })
        if (res.status_code == 200):
            try:
                base64_content = base64.b64encode(res.content).decode()
                return HttpResponse(json.dumps({'pdf': base64_content}))
            except Exception as e:
                print('Error while base64 encoding pdf content: {}'.format(str(e)))
                raise Exception(e)
        else:
            raise Exception('REQUEST TO PDF GENERATOR FAILED')
    except Exception as e:
        print('[ERROR] PDF generation error: {}'.format(str(e)))
        response = HttpResponse(json.dumps({
            "message": "error while generating results pdf",
            "code": "500"
        }))
        response.status_code = 400
        return response

from django.urls import path
from . import views

urlpatterns = [
    path("handler_hasura_action_npk/", views.handler_hasura_action_npk),
    path('action_handler_generate_results_pdf/', views.action_handler_generate_results_pdf),
]
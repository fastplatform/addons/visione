from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse

from constance.admin import ConstanceAdmin, Config

from .models import (
    Profile,
    ProfilePlantSpecies,
    ProfileStrategy,
    OrganicFertilizer,
    OrganicFertilizerPeriodAndMethodOfDistribution,
)



class ProfilePlantSpeciesInline(admin.TabularInline):
    model = ProfilePlantSpecies
    extra = 0


class ProfileStrategyInline(admin.StackedInline):
    model = ProfileStrategy
    extra = 0


class ProfileAdmin(admin.ModelAdmin):
    list_display = ("id", "_full_name", "profile_group")
    list_display_links = ("_full_name",)
    ordering = (
        "profile_name",
        "profile_option",
    )

    fieldsets = [
        (None, {"fields": ("profile_name", "profile_option", "profile_group")}),
        (_("Humidity"), {"fields": ("min_humidity", "humidity", "max_humidity")}),
        (_("Requirements"), {"fields": ("n", "p2o5", "k2o")}),
        (
            _("Standard dose"),
            {"fields": ("n_standard_dose", "p2o5_standard_dose", "k2o_standard_dose")},
        ),
        (
            _("Harvest & residues"),
            {
                "fields": (
                    "harvest_index",
                    "n_in_residues",
                    "p2o5_in_residues",
                    "k2o_in_residues",
                ),
            },
        ),
        (
            _("Parameters"),
            {
                "fields": (
                    "kt",
                    "n_fixation",
                    "mineralization_crop_residues",
                    "coef_reintegration",
                ),
            },
        ),
    ]

    def _full_name(self, obj):
        if obj.profile_option:
            return obj.profile_name + " / " + obj.profile_option
        else:
            return obj.profile_name

    _full_name.short_description = _("Profile")

    inlines = [ProfilePlantSpeciesInline, ProfileStrategyInline]


admin.site.register(Profile, ProfileAdmin)


class OrganicFertilizerPeriodAndMethodOfDistributionInline(admin.TabularInline):
    model = OrganicFertilizerPeriodAndMethodOfDistribution
    extra = 0
    fields = ["label", "period", "efficiency", "coef_efficiency"]
    ordering = ["label", "period"]


class OrganicFertilizerAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "name",
        "n",
        "p2o5",
        "k2o",
        "has_residual_effect",
    ]
    list_display_links = ["name"]
    ordering = ["name"]

    inlines = [OrganicFertilizerPeriodAndMethodOfDistributionInline]


admin.site.register(OrganicFertilizer, OrganicFertilizerAdmin)

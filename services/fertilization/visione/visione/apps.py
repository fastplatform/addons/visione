from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class VisioneConfig(AppConfig):
    name = "visione"
    verbose_name = _("Visione")

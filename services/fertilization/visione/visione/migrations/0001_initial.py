# Generated by Django 3.0.4 on 2021-02-15 09:18

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='OrganicFertilizer',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='identifier')),
                ('name', models.CharField(max_length=255, unique=True)),
                ('n', models.FloatField(help_text='Percentage of mass, between 0 and 100.', validators=[django.core.validators.MaxValueValidator(100), django.core.validators.MinValueValidator(0)], verbose_name='nitrogen (N)')),
                ('p2o5', models.FloatField(help_text='Percentage of mass, between 0 and 100.', validators=[django.core.validators.MaxValueValidator(100), django.core.validators.MinValueValidator(0)], verbose_name='Phosphorus (P2O5)')),
                ('k2o', models.FloatField(help_text='Percentage of mass, between 0 and 100.', validators=[django.core.validators.MaxValueValidator(100), django.core.validators.MinValueValidator(0)], verbose_name='Potassium (K20)')),
                ('has_residual_effect', models.BooleanField(verbose_name='has residual effect')),
            ],
            options={
                'verbose_name': 'organic fertilizer',
                'verbose_name_plural': 'organic fertilizers',
                'db_table': 'visione__organic_fertilizer',
            },
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='identifier')),
                ('profile_name', models.CharField(max_length=255)),
                ('profile_option', models.CharField(blank=True, max_length=255, null=True)),
                ('profile_group', models.CharField(choices=[], max_length=255)),
                ('humidity', models.FloatField(help_text='Percentage between 0 and 100.', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='Humidity')),
                ('min_humidity', models.FloatField(default=0, help_text='Percentage between 0 and 100.', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='Minimum humidity')),
                ('max_humidity', models.FloatField(default=100, help_text='Percentage between 0 and 100.', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='Maximum humidity')),
                ('n', models.FloatField(help_text='Percentage between 0 and 100.', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='N concentration')),
                ('p2o5', models.FloatField(help_text='Percentage between 0 and 100.', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='P2O5 concentration')),
                ('k2o', models.FloatField(help_text='Percentage between 0 and 100.', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='K2O concentration')),
                ('harvest_index', models.FloatField(help_text='Percentage between 0 and 100.', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='Harvest index')),
                ('n_in_residues', models.FloatField(help_text='Percentage between 0 and 100.', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='N concentration in in residues')),
                ('p2o5_in_residues', models.FloatField(help_text='Percentage between 0 and 100.', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='Phosphorus (P2O5) concentration in residues')),
                ('k2o_in_residues', models.FloatField(help_text='Percentage between 0 and 100.', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='Potassium (K20) concentration in residues')),
                ('kt', models.FloatField(help_text='Percentage between 0 and 100.', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='Kt time coefficient')),
                ('n_fixation', models.FloatField(help_text='Bfx coefficient, in kg N / ha.', verbose_name='Nitrogen fixation by legumes')),
                ('mineralization_crop_residues', models.FloatField(help_text='Mineralisation percentage of the crop residues of the preceding crop. Value between 0 and 100.', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='Mineralisation of crop residues')),
                ('coef_reintegration', models.FloatField(help_text='Coefficient to modulate P and K supply to PK availability in soil. Value between 0 and 100.', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)], verbose_name='Coefficient of reintegration')),
                ('n_standard_dose', models.FloatField(help_text='In kg/ha. Value between 0 and 1000.', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(1000)], verbose_name='Nitrogen standard dose')),
                ('p2o5_standard_dose', models.FloatField(help_text='In kg/ha. Value between 0 and 1000.', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(1000)], verbose_name='Phosphorus (P2O5) standard dose')),
                ('k2o_standard_dose', models.FloatField(help_text='In kg/ha. Value between 0 and 1000.', null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(1000)], verbose_name='Potassium (K2O) standard dose')),
            ],
            options={
                'verbose_name': 'Profile',
                'verbose_name_plural': 'Profiles',
                'db_table': 'visione__profile',
                'unique_together': {('profile_name', 'profile_option')},
            },
        ),
        migrations.CreateModel(
            name='ProfileStrategy',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='identifier')),
                ('period', models.CharField(max_length=255, verbose_name='period')),
                ('proposed_strategy', models.TextField(max_length=1000, verbose_name='proposed strategy')),
                ('profile', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='profile_strategies', to='visione.Profile', verbose_name='profile')),
            ],
            options={
                'verbose_name': 'profile strategy',
                'verbose_name_plural': 'profile strategies',
                'db_table': 'visione__profile_strategy',
                'unique_together': {('profile', 'period')},
            },
        ),
        migrations.CreateModel(
            name='ProfilePlantSpecies',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='identifier')),
                ('plant_species_id', models.CharField(help_text='Plant species identifier in the FaST Core database.', max_length=8, null=True)),
                ('profile', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='visione.Profile', verbose_name='profile')),
            ],
            options={
                'verbose_name': 'Profile plant species',
                'verbose_name_plural': 'Profiles plant species',
                'db_table': 'visione__profile_plant_species',
                'unique_together': {('profile', 'plant_species_id')},
            },
        ),
        migrations.CreateModel(
            name='OrganicFertilizerPeriodAndMethodOfDistribution',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='identifier')),
                ('label', models.CharField(max_length=255, verbose_name='label')),
                ('period', models.CharField(max_length=10, verbose_name='period')),
                ('efficiency', models.CharField(choices=[('bassa', 'bassa'), ('media', 'media'), ('alta', 'alta')], max_length=10, verbose_name='efficiency')),
                ('coef_efficiency', models.FloatField(validators=[django.core.validators.MaxValueValidator(100), django.core.validators.MinValueValidator(0)], verbose_name='efficiency coefficient')),
                ('organic_fertilizer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='visione.OrganicFertilizer')),
            ],
            options={
                'verbose_name': 'organic fertilizer N efficiency',
                'verbose_name_plural': 'organic fertilizer N efficiencies',
                'db_table': 'visione__organic_fertilizer_efficiency',
                'unique_together': {('organic_fertilizer', 'label')},
            },
        ),
    ]

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MaxValueValidator, MinValueValidator

from visione.lib.objects import (
    ProfileGroup,
    USDATexture,
    ManureDistribution,
    ProductionPhase,
)


class ProfileGroupChoices(ProfileGroup, models.TextChoices):
    pass


class Profile(models.Model):

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    profile_name = models.CharField(
        null=False, blank=False, max_length=255, verbose_name=_("profile name")
    )

    profile_option = models.CharField(
        null=True, blank=True, max_length=255, verbose_name=_("profile option")
    )

    profile_group = models.CharField(
        null=False,
        blank=False,
        max_length=255,
        choices=ProfileGroupChoices.choices,
        verbose_name=_("profile group"),
    )

    humidity = models.FloatField(
        verbose_name=_("Humidity"),
        help_text=_("Percentage between 0 and 100."),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )

    min_humidity = models.FloatField(
        default=0,
        verbose_name=_("Minimum humidity"),
        help_text=_("Percentage between 0 and 100."),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )

    max_humidity = models.FloatField(
        default=100,
        verbose_name=_("Maximum humidity"),
        help_text=_("Percentage between 0 and 100."),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )

    n = models.FloatField(
        verbose_name=_("N concentration"),
        help_text=_("Percentage between 0 and 100."),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )

    p2o5 = models.FloatField(
        verbose_name=_("P2O5 concentration"),
        help_text=_("Percentage between 0 and 100."),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )

    k2o = models.FloatField(
        verbose_name=_("K2O concentration"),
        help_text=_("Percentage between 0 and 100."),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )
    harvest_index = models.FloatField(
        verbose_name=_("Harvest index"),
        help_text=_("Percentage between 0 and 100."),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )

    n_in_residues = models.FloatField(
        verbose_name=_("N concentration in in residues"),
        help_text=_("Percentage between 0 and 100."),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )

    p2o5_in_residues = models.FloatField(
        verbose_name=_("Phosphorus (P2O5) concentration in residues"),
        help_text=_("Percentage between 0 and 100."),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )

    k2o_in_residues = models.FloatField(
        verbose_name=_("Potassium (K20) concentration in residues"),
        help_text=_("Percentage between 0 and 100."),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )

    kt = models.FloatField(
        verbose_name=_("Kt time coefficient"),
        help_text=_("Percentage between 0 and 100."),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )

    n_fixation = models.FloatField(
        verbose_name=_("Nitrogen fixation by legumes"),
        help_text=_("Bfx coefficient, in kg N / ha."),
    )

    mineralization_crop_residues = models.FloatField(
        null=True,
        verbose_name=_("Mineralisation of crop residues"),
        help_text=_(
            "Mineralisation percentage of the crop residues of the preceding crop. Value between 0 and 100."
        ),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )

    coef_reintegration = models.FloatField(
        null=True,
        verbose_name=_("Coefficient of reintegration"),
        help_text=_(
            "Coefficient to modulate P and K supply to PK availability in soil. Value between 0 and 100."
        ),
        validators=[MinValueValidator(0), MaxValueValidator(100)],
    )

    n_standard_dose = models.FloatField(
        null=True,
        verbose_name=_("Nitrogen standard dose"),
        help_text=_("In kg/ha. Value between 0 and 1000."),
        validators=[MinValueValidator(0), MaxValueValidator(1000)],
    )

    p2o5_standard_dose = models.FloatField(
        null=True,
        verbose_name=_("Phosphorus (P2O5) standard dose"),
        help_text=_("In kg/ha. Value between 0 and 1000."),
        validators=[MinValueValidator(0), MaxValueValidator(1000)],
    )

    k2o_standard_dose = models.FloatField(
        null=True,
        verbose_name=_("Potassium (K2O) standard dose"),
        help_text=_("In kg/ha. Value between 0 and 1000."),
        validators=[MinValueValidator(0), MaxValueValidator(1000)],
    )

    def __str__(self):
        return self.profile_name

    class Meta:
        db_table = "visione__profile"
        verbose_name = _("Profile")
        verbose_name_plural = _("Profiles")
        unique_together = (("profile_name", "profile_option"),)


class ProfilePlantSpecies(models.Model):

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    profile = models.ForeignKey(
        Profile, on_delete=models.CASCADE, verbose_name=_("profile")
    )

    plant_species_id = models.CharField(
        null=True,
        max_length=8,
        verbose_name=_("plant species identifier (in FaST)"),
        help_text=_("Plant species identifier in the FaST Core database."),
    )

    class Meta:
        db_table = "visione__profile_plant_species"
        verbose_name = _("Profile plant species")
        verbose_name_plural = _("Profiles plant species")
        unique_together = ("profile", "plant_species_id")


class ProfileStrategy(models.Model):

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    profile = models.ForeignKey(
        Profile,
        on_delete=models.CASCADE,
        verbose_name=_("profile"),
        related_name="profile_strategies",
    )

    period = models.CharField(
        null=False, blank=False, max_length=255, verbose_name=_("period")
    )

    proposed_strategy = models.TextField(
        null=False, blank=False, max_length=1000, verbose_name=_("proposed strategy")
    )

    class Meta:
        db_table = "visione__profile_strategy"
        verbose_name = _("profile strategy")
        verbose_name_plural = _("profile strategies")
        unique_together = ("profile", "period")


class OrganicFertilizer(models.Model):

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    name = models.CharField(max_length=255, unique=True, verbose_name=_("name"))

    n = models.FloatField(
        verbose_name=_("nitrogen (N)"),
        null=False,
        blank=False,
        validators=[MaxValueValidator(100), MinValueValidator(0)],
        help_text=_("""Percentage of mass, between 0 and 100."""),
    )

    p2o5 = models.FloatField(
        verbose_name=_("Phosphorus (P2O5)"),
        null=False,
        blank=False,
        validators=[MaxValueValidator(100), MinValueValidator(0)],
        help_text=_("""Percentage of mass, between 0 and 100."""),
    )

    k2o = models.FloatField(
        verbose_name=_("Potassium (K20)"),
        null=False,
        blank=False,
        validators=[MaxValueValidator(100), MinValueValidator(0)],
        help_text=_("""Percentage of mass, between 0 and 100."""),
    )

    has_residual_effect = models.BooleanField(verbose_name=_("has residual effect"))

    def __str__(self):
        return self.name

    class Meta:
        db_table = "visione__organic_fertilizer"
        verbose_name = _("organic fertilizer")
        verbose_name_plural = _("organic fertilizers")


class OrganicFertilizerEfficiencyChoices(models.TextChoices):
    bassa = "bassa", "bassa"
    media = "media", "media"
    alta = "alta", "alta"


class OrganicFertilizerPeriodAndMethodOfDistribution(models.Model):

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    organic_fertilizer = models.ForeignKey(
        OrganicFertilizer,
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        verbose_name=_("organic fertilizer"),
    )

    label = models.CharField(
        max_length=255, null=False, blank=False, verbose_name=_("label")
    )

    period = models.CharField(
        max_length=10, null=False, blank=False, verbose_name=_("period")
    )

    efficiency = models.CharField(
        max_length=10,
        null=False,
        blank=False,
        choices=OrganicFertilizerEfficiencyChoices.choices,
        verbose_name=_("efficiency"),
    )

    coef_efficiency = models.FloatField(
        null=False,
        blank=False,
        validators=[MaxValueValidator(100), MinValueValidator(0)],
        verbose_name=_("efficiency coefficient"),
    )

    def __str__(self):
        return self.label

    class Meta:
        db_table = "visione__organic_fertilizer_efficiency"
        verbose_name = _("organic fertilizer N efficiency")
        verbose_name_plural = _("organic fertilizer N efficiencies")
        unique_together = (
            "organic_fertilizer",
            "label",
        )


class USDATextureChoices(USDATexture, models.TextChoices):
    pass


class ManureDistributionChoices(ManureDistribution, models.TextChoices):
    pass


class ProductionPhaseChoices(ProductionPhase, models.TextChoices):
    pass

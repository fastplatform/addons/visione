import os
from pathlib import Path

from django.utils.translation import ugettext_lazy as _

BASE_DIR = Path(__file__).parent.parent

SECRET_KEY = os.environ.get("DJANGO_SECRET_KEY")
DEBUG = os.environ.get("DEBUG", "TRUE").upper() == "TRUE"
DOMAIN_NAME = os.environ.get("DOMAIN_NAME")
ALLOWED_HOSTS = [DOMAIN_NAME] + os.environ.get("DJANGO_ALLOWED_HOSTS", "").split(",")
if DEBUG:
    ALLOWED_HOSTS = [
        "localhost",
        "localhost.fastplatform.eu",
        "host.docker.internal",
    ] + ALLOWED_HOSTS
    CORS_ALLOW_ALL_ORIGINS = True
    INTERNAL_IPS = ["127.0.0.1", "192.168.1.33"]


SILENCED_SYSTEM_CHECKS = ["admin.E410"]

# Application definition
# ----------------------
INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "constance.backends.database",
    "config.apps.CustomConstanceConfig",
    "visione.apps.VisioneConfig",
]

if DEBUG:
    INSTALLED_APPS += [
        "sslserver",
        "django_extensions",
        "corsheaders",
        "debug_toolbar",
    ]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

if DEBUG:
    MIDDLEWARE += [
        "debug_toolbar.middleware.DebugToolbarMiddleware",
        "corsheaders.middleware.CorsMiddleware",
    ]

WSGI_APPLICATION = "config.wsgi.application"
ROOT_URLCONF = "config.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [BASE_DIR / "templates"],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "django.template.context_processors.i18n",
            ],
        },
    },
]

# Database
# --------
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": os.environ.get("POSTGRES_DATABASE", "visione"),
        "USER": os.environ.get("POSTGRES_USER", "fast"),
        "PASSWORD": os.environ.get("POSTGRES_PASSWORD", "fast"),
        "HOST": os.environ.get("POSTGRES_HOST", "localhost"),
        "PORT": os.environ.get("POSTGRES_PORT", "5440"),
        "CONN_MAX_AGE": int(os.environ.get("POSTGRES_CONN_MAX_AGE", 0)),
        "DISABLE_SERVER_SIDE_CURSORS": os.environ.get("DISABLE_SERVER_SIDE_CURSORS", "TRUE").upper() == "TRUE",
    }
}

DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

# Internationalization
# --------------------
LANGUAGE_CODE = os.environ.get("LANGUAGE_CODE", "en")

LANGUAGES = (
    ("bg", "Bulgarian / Български"),
    ("el", "Greek / Ελληνική"),
    ("en", "English"),
    ("es", "Spanish / Castellano"),
    ("et", "Estonian / Eesti"),
    ("fr", "French / Français"),
    ("it", "Italian / Italiano"),
    ("ro", "Romanian / Română"),
    ("sk", "Slovak / Slovenčina"),
)
TIME_ZONE = os.environ.get("TIME_ZONE", "Europe/Rome")
USE_I18N = True
USE_L10N = True
USE_TZ = True

LOCALE_PATHS = (BASE_DIR / "locale",)

# Static files
# ------------
# Static files are served by whitenoise
STATIC_URL = "/static/"
STATIC_ROOT = BASE_DIR / "static"
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]

# Messages
# --------
MESSAGE_STORAGE = "django.contrib.messages.storage.session.SessionStorage"

# Constance
# ---------
from visione.config import VISIONE_CONFIG

CONSTANCE_CONFIG = VISIONE_CONFIG
CONSTANCE_BACKEND = "constance.backends.database.DatabaseBackend"
CONSTANCE_IGNORE_ADMIN_VERSION_CHECK = True

# Security
# --------
SECURE_HSTS_SECONDS = int(os.environ.get("DJANGO_SECURE_HSTS_SECONDS", 0))
if SECURE_HSTS_SECONDS > 0:
    SECURE_HSTS_INCLUDE_SUBDOMAINS = True
    SECURE_HSTS_PRELOAD = True

SECURE_REFERRER_POLICY = "same-origin"
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
USE_X_FORWARDED_HOST = True
SECURE_REFERRER_POLICY = "same-origin"
CSRF_COOKIE_SECURE = (
    os.environ.get("DJANGO_CSRF_COOKIE_SECURE", "TRUE").upper() == "TRUE"
)

# PDF Generator
# -------------
PDF_GENERATOR_ENDPOINT = os.environ.get(
    "PDF_GENERATOR_ENDPOINT", "http://0.0.0.0:7010/generate_pdf"
)

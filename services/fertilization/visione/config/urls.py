from django.contrib import admin
from django.urls import include, path
from django.views.generic import RedirectView
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.conf.urls.i18n import i18n_patterns

admin.site.site_title = _("Visione")
admin.site.site_header = _("Visione")
admin.site.index_title = _("Management console")
admin.site.site_url = "/admin"

urlpatterns = [
    path("", RedirectView.as_view(pattern_name="admin:index")),
    path("i18n/", include("django.conf.urls.i18n")),
    path("visione/", include("visione.urls")),
] + i18n_patterns(path("admin/", admin.site.urls))

if settings.DEBUG:
    import debug_toolbar
    
    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]

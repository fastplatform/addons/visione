# fertilization/visione

```fertilization/visione``` is a [Django](https://www.djangoproject.com/) service that exposes endpoints that:
-  compute the NPK recommendation. This endpoint is not exposed directly and is encapsulated into a GraphQL mutation by the [visione_api_gateway](../visione_api_gateway) service.
- allows the management (edition) of algorithm parameters, including crop profiles, manure chemical content and efficiency, and meta parameters.

The objects stored in the database are defined in the [api/models.py]() file.


```plantuml
class Profile {
    id
    profile_name
    profile_option
    profile_group
    humidity
    min_humidity
    max_humidity
    n
    p2o5
    k2o
    harvest_index
    n_in_residues
    p2o5_in_residues
    k2o_in_residues
    kt
    n_fixation
    mineralization_crop_residues
    coef_reintegration
    n_standard_dose
    p2o5_standard_dose
    k2o_standard_dose
}

class ProfilePlantSpecies {
    id
    profile
    plant_species_id
}

class PlantSpecies #white{
    id
}

class ProfileStrategy {
    id
    profile
    period
    proposed_strategy
}

Profile --> ProfilePlantSpecies
ProfilePlantSpecies -> PlantSpecies
Profile --> ProfileStrategy

class OrganicFertilizer {
    id
    name
    n
    p2o5
    k2o
    has_residual_effect
}

class OrganicFertilizerPeriodAndMethodOfDistribution {
    id
    organic_fertilizer
    label
    period
    efficiency
    coef_efficiency
}

OrganicFertilizer --> OrganicFertilizerPeriodAndMethodOfDistribution
```

## Prerequisites

- Python 3.7+ and [`virtualenv`](https://virtualenv.pypa.io/en/latest/)

## Environment variables

- `API_GATEWAY_FASTPLATFORM_URL`: URL of the [fastplatform](https://gitlab.com/fastplatform/core/-/tree/master/services/api_gateway/fastplatform) Hasura API Gateway
- `DJANGO_SECRET_KEY`: Django secret key
- `LOG_LEVEL`: logging level (trace, debug, info, warn, error, fatal)
- `POSTGRES_DATABASE`: database name of Visione Postgres database
- `POSTGRES_HOST`: host of Visione Postgres database
- `POSTGRES_PORT`: port of Visione Postgres database
- `POSTGRES_USER`: user of Visione Postgres database
- `POSTGRES_PASSWORD`: password of Visione Postgres database

All settings are listed in the [backend/settings.py]() configuration file.

## Development Setup

Create a Python virtualenv and activate it:
```
virtualenv .venv
source .venv/bin/activate
```

Install Python dependencies:
```
pip install -r requirements.txt

# Optionally install dev packages
pip install -r requirements-dev.txt
```

If needed start monitoring OpenTelemetry traces
```
make start-tracing
```
Jaeger UI will be available at [http://localhost:16686](http://localhost:16686)

Start the service:
```bash
make start-postgres # Start a local Postgres database
make migrations # Generate Django migrations
make migrate # Apply Django migrations
make init # Initialize the local Postgres database
make start
```

The API server is now started and available at [http://localhost:8100](http://localhost:8100):
- management backend -> [http://localhost:8101/admin/](http://localhost:8101/admin/)
- recommendation endpoint -> [http://localhost:8101/visione/handler_hasura_action_npk/](http://localhost:8101/visione/handler_hasura_action_npk/).

**Server can be also started with Bazel** (no need to activate a Python virtualenv):
```bash
$ make bazel-run 
$ make bazel-run-init # Initialize the local Postgres database if needed (to be executed from an other shell)
```

## Tests

```bash
make test
```

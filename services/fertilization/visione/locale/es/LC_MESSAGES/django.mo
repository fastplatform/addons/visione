��    <      �  S   �      (     )     H     X  [   u     �     �     �        #   	     -     ?     S     [     n       ^   �     �               >     [     r  
   �     �  &   �     �  +   �       3   3     g  )   w     �     �     �     �     �     �     
	     	      0	     Q	  	   p	     z	     �	  
   �	     �	     �	  
   �	     �	     �	     �	  !   �	     
     4
     H
     O
     W
     j
     {
  k  �
     �          ,  j   J     �     �     �     �     �          (     A     S     g  +   w  c   �            #   +  +   O     {     �     �     �  %   �     �  1     "   >  B   a     �  /   �      �          
     '     0  
   M     X      h     �     �  	   �     �     �  
   �     �               (     1     @  ,   P  0   }     �     �     �     �     �     �             &   6   -   +      /   7   %             1   )             :      ;                  '   $          5       ,      "                       !              8                        <      *         2       #         0                     9   .   	              3         (       
          4    Bfx coefficient, in kg N / ha. Change password Coefficient of reintegration Coefficient to modulate P and K supply to PK availability in soil. Value between 0 and 100. Documentation Harvest & residues Harvest index Humidity In kg/ha. Value between 0 and 1000. K2O concentration Kt time coefficient Log out Management console Maximum humidity Mineralisation of crop residues Mineralisation percentage of the crop residues of the preceding crop. Value between 0 and 100. Minimum humidity N concentration N concentration in in residues Nitrogen fixation by legumes Nitrogen standard dose P2O5 concentration Parameters Percentage between 0 and 100. Percentage of mass, between 0 and 100. Phosphorus (P2O5) Phosphorus (P2O5) concentration in residues Phosphorus (P2O5) standard dose Plant species identifier in the FaST Core database. Potassium (K20) Potassium (K20) concentration in residues Potassium (K2O) standard dose Profile Profile plant species Profiles Profiles plant species Requirements Standard dose Target nitrogen surplus Target phosphorus (P2O5) surplus Target potassium (K20) surplus View site Visione Welcome, efficiency efficiency coefficient has residual effect identifier label nitrogen (N) organic fertilizer organic fertilizer N efficiencies organic fertilizer N efficiency organic fertilizers period profile profile strategies profile strategy proposed strategy Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-28 18:51+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Coeficiente Bfx, en kg N / ha. Cambiar contraseña Coeficiente de reintegración Coeficiente de modulación del aporte de P y K a la disponibilidad de PK en el suelo. Valor entre 0 y 100. Documentación Cosecha y residuos Índice de cosecha Humedad En kg/ha. Valor entre 0 y 1000. Concentración de K2O Coeficiente de tiempo Kt Cerrar la sesión Consola de gestión Humedad máxima Mineralización de los residuos de cultivos Porcentaje de mineralización de los residuos de cultivo del cultivo anterior. Valor entre 0 y 100. Humedad mínima Concentración de N Concentración de N en los residuos Fijación de nitrógeno por las leguminosas Dosis estándar de nitrógeno Concentración de P2O5 Parámetros Porcentaje entre 0 y 100. Porcentaje de la masa, entre 0 y 100. Fósforo (P2O5) Concentración de fósforo (P2O5) en los residuos Dosis estándar de fósforo (P2O5) Identificador de la especie vegetal en la base de datos FaST Core. Potasio (K20) Concentración de potasio (K20) en los residuos Dosis estándar de potasio (K2O) Perfil Perfil de la especie vegetal Perfiles Perfil de la especie vegetal Requisitos Dosis estándar Excedente de nitrógeno objetivo Excedente de fósforo (P2O5) Excedente de potasio (K20) Ver sitio Visione Bienvenido, eficiencia coeficiente de eficiencia tiene efecto residual identificador etiqueta nitrógeno (N) abono orgánico eficiencia N de los fertilizantes orgánicos eficiencia del N de los fertilizantes orgánicos abonos orgánicos período perfil estrategias de perfil estrategia de perfil estrategia propuesta 
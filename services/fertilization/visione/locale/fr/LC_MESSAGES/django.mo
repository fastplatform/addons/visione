��    <      �  S   �      (     )     H     X  [   u     �     �     �        #   	     -     ?     S     [     n       ^   �     �               >     [     r  
   �     �  &   �     �  +   �       3   3     g  )   w     �     �     �     �     �     �     
	     	      0	     Q	  	   p	     z	     �	  
   �	     �	     �	  
   �	     �	     �	     �	  !   �	     
     4
     H
     O
     W
     j
     {
  j  �
     �          /  �   N     �     �     �  	     *        E     Z     r     �     �  '   �  r   �     C     V  $   i  )   �     �     �     �  #   �  (        =  3   N  !   �  G   �     �  2   �      /     P     W     w       	   �     �     �  #   �  "   �          "  
   *     5     A     [     p  
   |  	   �     �  -   �  $   �     �     	               /     D             &   6   -   +      /   7   %             1   )             :      ;                  '   $          5       ,      "                       !              8                        <      *         2       #         0                     9   .   	              3         (       
          4    Bfx coefficient, in kg N / ha. Change password Coefficient of reintegration Coefficient to modulate P and K supply to PK availability in soil. Value between 0 and 100. Documentation Harvest & residues Harvest index Humidity In kg/ha. Value between 0 and 1000. K2O concentration Kt time coefficient Log out Management console Maximum humidity Mineralisation of crop residues Mineralisation percentage of the crop residues of the preceding crop. Value between 0 and 100. Minimum humidity N concentration N concentration in in residues Nitrogen fixation by legumes Nitrogen standard dose P2O5 concentration Parameters Percentage between 0 and 100. Percentage of mass, between 0 and 100. Phosphorus (P2O5) Phosphorus (P2O5) concentration in residues Phosphorus (P2O5) standard dose Plant species identifier in the FaST Core database. Potassium (K20) Potassium (K20) concentration in residues Potassium (K2O) standard dose Profile Profile plant species Profiles Profiles plant species Requirements Standard dose Target nitrogen surplus Target phosphorus (P2O5) surplus Target potassium (K20) surplus View site Visione Welcome, efficiency efficiency coefficient has residual effect identifier label nitrogen (N) organic fertilizer organic fertilizer N efficiencies organic fertilizer N efficiency organic fertilizers period profile profile strategies profile strategy proposed strategy Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-28 18:51+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Coefficient Bfx, en kg N / ha. Changer le mot de passe Coefficient de réintégration Coefficient permettant de moduler l'apport de P et de K en fonction de la disponibilité de PK dans le sol. Valeur comprise entre 0 et 100. Documentation Récolte et résidus Indice de récolte Humidité En kg/ha. Valeur comprise entre 0 et 1000. Concentration de K2O Coefficient de temps Kt Se déconnecter Console de gestion Humidité maximale Minéralisation des résidus de culture Pourcentage de minéralisation des résidus de culture de la culture précédente. Valeur comprise entre 0 et 100. Humidité minimale Concentration en N Concentration de N dans les résidus Fixation de l'azote par les légumineuses Dose standard d'azote Concentration de P2O5 Paramètres Pourcentage compris entre 0 et 100. Pourcentage de la masse, entre 0 et 100. Phosphore (P2O5) Concentration de phosphore (P2O5) dans les résidus Dose standard de phosphore (P2O5) Identifiant de l'espèce végétale dans la base de données FaST Core. Potassium (K20) Concentration de potassium (K20) dans les résidus Dose standard de potassium (K2O) Profil Profil des espèces végétales Profils Profils d'espèces végétales Exigences Dose standard Excédent d'azote visé Excédent de phosphore (P2O5) visé Excédent de potassium (K20) cible Voir le site Visione Bienvenue, efficacité coefficient d'efficacité a un effet résiduel Identifiant étiquette azote (N) engrais organique efficacité de l'azote des engrais organiques efficacité N des engrais organiques engrais organiques période profil stratégies de profil stratégie de profil stratégie proposée 
��    <      �  S   �      (     )     H     X  [   u     �     �     �        #   	     -     ?     S     [     n       ^   �     �               >     [     r  
   �     �  &   �     �  +   �       3   3     g  )   w     �     �     �     �     �     �     
	     	      0	     Q	  	   p	     z	     �	  
   �	     �	     �	  
   �	     �	     �	     �	  !   �	     
     4
     H
     O
     W
     j
     {
  k  �
  %   �  $     2   D  �   w     1  <   J  $   �     �  0   �  !   �  '     (   8  (   a  %   �  I   �  �   �  #   �     �  7   �  C   &  &   j  "   �     �  #   �  6   �     "  I   6  1   �  p   �     #  F   4  .   {     �  -   �     �  5   �     *     ?  (   ]  4   �  1   �     �               +  0   B  $   s     �     �     �     �  A   �  A   /     q     �     �  #   �  #   �  '   �             &   6   -   +      /   7   %             1   )             :      ;                  '   $          5       ,      "                       !              8                        <      *         2       #         0                     9   .   	              3         (       
          4    Bfx coefficient, in kg N / ha. Change password Coefficient of reintegration Coefficient to modulate P and K supply to PK availability in soil. Value between 0 and 100. Documentation Harvest & residues Harvest index Humidity In kg/ha. Value between 0 and 1000. K2O concentration Kt time coefficient Log out Management console Maximum humidity Mineralisation of crop residues Mineralisation percentage of the crop residues of the preceding crop. Value between 0 and 100. Minimum humidity N concentration N concentration in in residues Nitrogen fixation by legumes Nitrogen standard dose P2O5 concentration Parameters Percentage between 0 and 100. Percentage of mass, between 0 and 100. Phosphorus (P2O5) Phosphorus (P2O5) concentration in residues Phosphorus (P2O5) standard dose Plant species identifier in the FaST Core database. Potassium (K20) Potassium (K20) concentration in residues Potassium (K2O) standard dose Profile Profile plant species Profiles Profiles plant species Requirements Standard dose Target nitrogen surplus Target phosphorus (P2O5) surplus Target potassium (K20) surplus View site Visione Welcome, efficiency efficiency coefficient has residual effect identifier label nitrogen (N) organic fertilizer organic fertilizer N efficiencies organic fertilizer N efficiency organic fertilizers period profile profile strategies profile strategy proposed strategy Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-28 18:51+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Коефициент Bfx, в kg N/ha. Промяна на паролата Коефициент на реинтеграция Коефициент за модулиране на доставката на P и K спрямо наличността на PK в почвата. Стойност между 0 и 100. Документация Събиране на реколтата и остатъци Индекс на реколтата Влажност В kg/ha. Стойност между 0 и 1000. Концентрация на K2O Коефициент на време Kt Излизане от системата Конзола за управление Максимална влажност Минерализация на растителните остатъци Процент на минерализация на растителните остатъци от предходната култура. Стойност между 0 и 100. Минимална влажност Концентрация на N Концентрация на N в остатъците Фиксиране на азот от бобови растения Стандартна доза азот Концентрация на P2O5 Параметри Процент между 0 и 100. Процент от масата, между 0 и 100. Фосфор (P2O5) Концентрация на фосфор (P2O5) в остатъците Стандартна доза фосфор (P2O5) Идентификатор на растителен вид в основната база данни на FaST. Калий (K20) Концентрация на калий (K20) в остатъците Стандартна доза калий (K2O) Профил Профил на растителен вид Профили Профили на растителни видове Изисквания Стандартна доза Целеви азотен излишък Целеви излишък на фосфор (P2O5) Целеви излишък на калий (K20) Преглед на сайта Visione Добре дошли, ефективност коефициент на ефективност има остатъчен ефект идентификатор етикет азот (N) органични торове N ефективност на органичните торове ефективност на органичните торове N органични торове период профил профилни стратегии профилна стратегия предложена стратегия 
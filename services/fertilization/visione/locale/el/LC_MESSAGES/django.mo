��    <      �  S   �      (     )     H     X  [   u     �     �     �        #   	     -     ?     S     [     n       ^   �     �               >     [     r  
   �     �  &   �     �  +   �       3   3     g  )   w     �     �     �     �     �     �     
	     	      0	     Q	  	   p	     z	     �	  
   �	     �	     �	  
   �	     �	     �	     �	  !   �	     
     4
     H
     O
     W
     j
     {
  k  �
  +   �  .   %  -   T  �   �     <  +   Q  #   }     �  0   �     �  &   �     #  %   8     ^  J   |  �   �     s     �  5   �  3   �  $        <     X  )   m  5   �     �  L   �  /   2  d   b     �  H   �  (   !     J  (   W     �  &   �     �     �  ,   �  7     3   D  %   x     �     �  $   �  '   �  4        A     \     k     {  O   �  9   �  #   %     I     Z  #   g  !   �  -   �             &   6   -   +      /   7   %             1   )             :      ;                  '   $          5       ,      "                       !              8                        <      *         2       #         0                     9   .   	              3         (       
          4    Bfx coefficient, in kg N / ha. Change password Coefficient of reintegration Coefficient to modulate P and K supply to PK availability in soil. Value between 0 and 100. Documentation Harvest & residues Harvest index Humidity In kg/ha. Value between 0 and 1000. K2O concentration Kt time coefficient Log out Management console Maximum humidity Mineralisation of crop residues Mineralisation percentage of the crop residues of the preceding crop. Value between 0 and 100. Minimum humidity N concentration N concentration in in residues Nitrogen fixation by legumes Nitrogen standard dose P2O5 concentration Parameters Percentage between 0 and 100. Percentage of mass, between 0 and 100. Phosphorus (P2O5) Phosphorus (P2O5) concentration in residues Phosphorus (P2O5) standard dose Plant species identifier in the FaST Core database. Potassium (K20) Potassium (K20) concentration in residues Potassium (K2O) standard dose Profile Profile plant species Profiles Profiles plant species Requirements Standard dose Target nitrogen surplus Target phosphorus (P2O5) surplus Target potassium (K20) surplus View site Visione Welcome, efficiency efficiency coefficient has residual effect identifier label nitrogen (N) organic fertilizer organic fertilizer N efficiencies organic fertilizer N efficiency organic fertilizers period profile profile strategies profile strategy proposed strategy Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-28 18:51+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Συντελεστής Bfx, σε kg N / ha. Αλλαγή κωδικού πρόσβασης Συντελεστής επανένταξης Συντελεστής για τη διαμόρφωση της παροχής P και K με τη διαθεσιμότητα PK στο έδαφος. Τιμή μεταξύ 0 και 100. Τεκμηρίωση Συγκομιδή & υπολείμματα Δείκτης συγκομιδής Υγρασία Σε kg/ha. Τιμή μεταξύ 0 και 1000. Συγκέντρωση K2O Συντελεστής χρόνου Kt Αποσύνδεση Κονσόλα διαχείρισης Μέγιστη υγρασία Ορυκτοποίηση υπολειμμάτων καλλιεργειών Ποσοστό ανοργανοποίησης των υπολειμμάτων της προηγούμενης καλλιέργειας. Τιμή μεταξύ 0 και 100. Ελάχιστη υγρασία Συγκέντρωση Ν Συγκέντρωση Ν σε υπολείμματα Δέσμευση αζώτου από ψυχανθή Πρότυπη δόση αζώτου Συγκέντρωση P2O5 Παράμετροι Ποσοστό μεταξύ 0 και 100. Ποσοστό μάζας, μεταξύ 0 και 100. Φώσφορος (P2O5) Συγκέντρωση φωσφόρου (P2O5) στα υπολείμματα Πρότυπη δόση φωσφόρου (P2O5) Αναγνωριστικό φυτικού είδους στη βάση δεδομένων FaST Core. Κάλιο (K20) Συγκέντρωση καλίου (Κ20) στα υπολείμματα Τυπική δόση καλίου (K2O) Προφίλ Προφίλ φυτικού είδους Προφίλ Προφίλ φυτικών ειδών Απαιτήσεις Τυπική δόση Στόχος πλεόνασμα αζώτου Στόχος πλεόνασμα φωσφόρου (P2O5) Στόχος πλεόνασμα καλίου (Κ20) Προβολή ιστοσελίδας Visione Καλώς ήρθατε, αποτελεσματικότητα συντελεστής απόδοσης έχει υπολειμματική επίδραση αναγνωριστικό ετικέτα άζωτο (Ν) Οργανικό λίπασμα αποτελεσματικότητα οργανικών λιπασμάτων Ν απόδοση οργανικών λιπασμάτων Ν Οργανικά λιπάσματα περίοδος προφίλ στρατηγικές προφίλ στρατηγική προφίλ προτεινόμενη στρατηγική 
��    <      �  S   �      (     )     H     X  [   u     �     �     �        #   	     -     ?     S     [     n       ^   �     �               >     [     r  
   �     �  &   �     �  +   �       3   3     g  )   w     �     �     �     �     �     �     
	     	      0	     Q	  	   p	     z	     �	  
   �	     �	     �	  
   �	     �	     �	     �	  !   �	     
     4
     H
     O
     W
     j
     {
  k  �
     �          #  c   @     �     �     �     �  ;   �          -     D     S     c      w  W   �     �  
          *   $     O     h     ~     �  +   �     �  *   �     	  0   %     V  *   d     �     �     �  	   �     �     �     �       "   "     E     c     o     w     �     �     �     �     �     �     �  !   �  !        2     I     Q     Y     n     �             &   6   -   +      /   7   %             1   )             :      ;                  '   $          5       ,      "                       !              8                        <      *         2       #         0                     9   .   	              3         (       
          4    Bfx coefficient, in kg N / ha. Change password Coefficient of reintegration Coefficient to modulate P and K supply to PK availability in soil. Value between 0 and 100. Documentation Harvest & residues Harvest index Humidity In kg/ha. Value between 0 and 1000. K2O concentration Kt time coefficient Log out Management console Maximum humidity Mineralisation of crop residues Mineralisation percentage of the crop residues of the preceding crop. Value between 0 and 100. Minimum humidity N concentration N concentration in in residues Nitrogen fixation by legumes Nitrogen standard dose P2O5 concentration Parameters Percentage between 0 and 100. Percentage of mass, between 0 and 100. Phosphorus (P2O5) Phosphorus (P2O5) concentration in residues Phosphorus (P2O5) standard dose Plant species identifier in the FaST Core database. Potassium (K20) Potassium (K20) concentration in residues Potassium (K2O) standard dose Profile Profile plant species Profiles Profiles plant species Requirements Standard dose Target nitrogen surplus Target phosphorus (P2O5) surplus Target potassium (K20) surplus View site Visione Welcome, efficiency efficiency coefficient has residual effect identifier label nitrogen (N) organic fertilizer organic fertilizer N efficiencies organic fertilizer N efficiency organic fertilizers period profile profile strategies profile strategy proposed strategy Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-28 18:51+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Bfx koefitsient, kg N/ha. Muuda salasõna Reintegratsiooni koefitsient Koefitsient P ja K pakkumise ja PK kättesaadavuse muutmiseks mullas. Väärtus vahemikus 0 ja 100. Dokumentatsioon Saak ja jäägid Saagikusindeks Niiskus Kilogrammides hektari kohta. Väärtus vahemikus 0 ja 1000. K2O kontsentratsioon Kt ajaline koefitsient Väljalogimine Juhtimiskonsool Maksimaalne niiskus Taimejääkide mineraliseerumine Eelmise põllukultuuri jääkide mineraliseerumise protsent. Väärtus vahemikus 0-100. Minimaalne niiskus N sisaldus N sisaldus jääkides Lämmastiku sidumine liblikõieliste poolt Lämmastiku standarddoos P2O5 kontsentratsioon Parameetrid Protsent vahemikus 0-100. Protsent massiprotsent, vahemikus 0 ja 100. Fosfor (P2O5) Fosfori (P2O5) kontsentratsioon jääkides Fosfori (P2O5) standarddoos Taimeliigi identifikaator FaST Core andmebaasis. Kaalium (K20) Kaaliumi (K20) kontsentratsioon jääkides Kaaliumi (K2O) standarddoos. Profiil Taimeliigi profiil Profiilid Profiilid taimeliikide kohta Nõuded Standardne annus Sihtlämmastiku ülejääk Eesmärk fosfori (P2O5) ülejääk Sihtkaaliumi (K20) ülejääk Vaata saiti Visione Tere tulemast, tõhusus tõhususe koefitsient on jääkmõju identifikaator etikett lämmastik (N) orgaaniline väetis orgaaniliste väetiste N tõhusus orgaaniliste väetiste N tõhusus orgaanilised väetised periood profiil profiili strateegiad profiili strateegia kavandatav strateegia 
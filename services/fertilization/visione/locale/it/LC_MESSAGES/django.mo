��    <      �  S   �      (     )     H     X  [   u     �     �     �        #   	     -     ?     S     [     n       ^   �     �               >     [     r  
   �     �  &   �     �  +   �       3   3     g  )   w     �     �     �     �     �     �     
	     	      0	     Q	  	   p	     z	     �	  
   �	     �	     �	  
   �	     �	     �	     �	  !   �	     
     4
     H
     O
     W
     j
     {
  k  �
     �          )  n   H     �     �     �     �  '   �          3     L     [     o  &   �  l   �          $     5  /   U     �     �  	   �     �  %   �     �  ,        5  =   R     �  ,   �     �     �     �            	   .     8     F  "   `  "   �     �     �  
   �  
   �     �     �       	     	        (  '   ?  '   g     �     �     �     �     �     �             &   6   -   +      /   7   %             1   )             :      ;                  '   $          5       ,      "                       !              8                        <      *         2       #         0                     9   .   	              3         (       
          4    Bfx coefficient, in kg N / ha. Change password Coefficient of reintegration Coefficient to modulate P and K supply to PK availability in soil. Value between 0 and 100. Documentation Harvest & residues Harvest index Humidity In kg/ha. Value between 0 and 1000. K2O concentration Kt time coefficient Log out Management console Maximum humidity Mineralisation of crop residues Mineralisation percentage of the crop residues of the preceding crop. Value between 0 and 100. Minimum humidity N concentration N concentration in in residues Nitrogen fixation by legumes Nitrogen standard dose P2O5 concentration Parameters Percentage between 0 and 100. Percentage of mass, between 0 and 100. Phosphorus (P2O5) Phosphorus (P2O5) concentration in residues Phosphorus (P2O5) standard dose Plant species identifier in the FaST Core database. Potassium (K20) Potassium (K20) concentration in residues Potassium (K2O) standard dose Profile Profile plant species Profiles Profiles plant species Requirements Standard dose Target nitrogen surplus Target phosphorus (P2O5) surplus Target potassium (K20) surplus View site Visione Welcome, efficiency efficiency coefficient has residual effect identifier label nitrogen (N) organic fertilizer organic fertilizer N efficiencies organic fertilizer N efficiency organic fertilizers period profile profile strategies profile strategy proposed strategy Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-28 18:51+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Bfx coefficiente, in kg N / ha. Cambia password Coefficiente di reintegrazione Coefficiente per modulare l'apporto di P e K alla disponibilità di PK nel suolo. Valore compreso tra 0 e 100. Documentazione Raccolta e residui Indice di raccolta Umidità In kg/ha. Valore compreso tra 0 e 1000. Concentrazione di K2O Coefficiente di tempo Kt Disconnettersi Console di gestione Umidità massima Mineralizzazione dei residui colturali Percentuale di mineralizzazione dei residui colturali della coltura precedente. Valore compreso tra 0 e 100. Umidità minima Concentrazione N Concentrazione di N nei residui Fissazione dell'azoto da parte delle leguminose Dose standard di azoto Concentrazione P2O5 Parametri Percentuale tra 0 e 100. Percentuale della massa, tra 0 e 100. Fosforo (P2O5) Concentrazione di fosforo (P2O5) nei residui Fosforo (P2O5) dose standard Identificatore delle specie di piante nel database FaST Core. Potassio (K20) Concentrazione di potassio (K20) nei residui Dose standard di potassio (K2O) Profilo Profilo specie di piante Profili Profili specie di piante Requisiti Dose standard Eccedenza target di azoto Eccedenza target di fosforo (P2O5) Eccedenza target di potassio (K20) Visualizza il sito Visione Benvenuto, efficienza coefficiente di efficienza ha effetto residuo identificatore etichetta azoto (N) fertilizzante organico efficienza N del fertilizzante organico efficienza N del fertilizzante organico fertilizzanti organici periodo profilo strategie di profilo profilo strategia strategia proposta 
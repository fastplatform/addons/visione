��    <      �  S   �      (     )     H     X  [   u     �     �     �        #   	     -     ?     S     [     n       ^   �     �               >     [     r  
   �     �  &   �     �  +   �       3   3     g  )   w     �     �     �     �     �     �     
	     	      0	     Q	  	   p	     z	     �	  
   �	     �	     �	  
   �	     �	     �	     �	  !   �	     
     4
     H
     O
     W
     j
     {
  �  �
     D     _     k  V   �     �     �     �                /     A     X     f     y  $   �  _   �          &     6     T     r     �  	   �  !   �      �     �  *   �  "   '  >   J     �  +   �  #   �     �     �     	          -     9     M  !   h  "   �     �     �     �     �     �     �       
     
   '     2  "   E  "   h     �     �     �     �     �     �             &   6   -   +      /   7   %             1   )             :      ;                  '   $          5       ,      "                       !              8                        <      *         2       #         0                     9   .   	              3         (       
          4    Bfx coefficient, in kg N / ha. Change password Coefficient of reintegration Coefficient to modulate P and K supply to PK availability in soil. Value between 0 and 100. Documentation Harvest & residues Harvest index Humidity In kg/ha. Value between 0 and 1000. K2O concentration Kt time coefficient Log out Management console Maximum humidity Mineralisation of crop residues Mineralisation percentage of the crop residues of the preceding crop. Value between 0 and 100. Minimum humidity N concentration N concentration in in residues Nitrogen fixation by legumes Nitrogen standard dose P2O5 concentration Parameters Percentage between 0 and 100. Percentage of mass, between 0 and 100. Phosphorus (P2O5) Phosphorus (P2O5) concentration in residues Phosphorus (P2O5) standard dose Plant species identifier in the FaST Core database. Potassium (K20) Potassium (K20) concentration in residues Potassium (K2O) standard dose Profile Profile plant species Profiles Profiles plant species Requirements Standard dose Target nitrogen surplus Target phosphorus (P2O5) surplus Target potassium (K20) surplus View site Visione Welcome, efficiency efficiency coefficient has residual effect identifier label nitrogen (N) organic fertilizer organic fertilizer N efficiencies organic fertilizer N efficiency organic fertilizers period profile profile strategies profile strategy proposed strategy Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-28 18:51+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n == 1 ? 0 : n % 1 == 0 && n >= 2 && n <= 4 ? 1 : n % 1 != 0 ? 2: 3);
 Koeficient Bfx, v kg N/ha. Zmena hesla Koeficient reintegrácie Koeficient na úpravu dodávky P a K na dostupnosť PK v pôde. Hodnota medzi 0 a 100. Dokumentácia Zber a zvyšky Index úrody Vlhkosť V kg/ha. Hodnota medzi 0 a 1000. Koncentrácia K2O Časový koeficient Kt Odhlásiť sa Konzola na správu Maximálna vlhkosť Mineralizácia rastlinných zvyškov Percento mineralizácie rastlinných zvyškov predchádzajúcej plodiny. Hodnota medzi 0 a 100. Minimálna vlhkosť Koncentrácia N Koncentrácia N v rezíduách Fixácia dusíka strukovinami Štandardná dávka dusíka Koncentrácia P2O5 Parametre Percentuálny podiel od 0 do 100. Percento hmotnosti, od 0 do 100. Fosfor (P2O5) Koncentrácia fosforu (P2O5) v rezíduách Štandardná dávka fosforu (P2O5) Identifikátor rastlinného druhu v základnej databáze FaST. Draslík (K20) Koncentrácia draslíka (K20) v rezíduách Štandardná dávka draslíka (K2O) Profil Profil rastlinného druhu Profily Profily rastlinných druhov Požiadavky Štandardná dávka Cieľový prebytok dusíka Cieľový prebytok fosforu (P2O5) Cieľový prebytok draslíka (K20) Zobraziť stránku Visione Vitajte, účinnosť koeficient účinnosti má reziduálny účinok identifikátor označenie dusík (N) organické hnojivo účinnosť N organických hnojív účinnosť organických hnojív N organické hnojivá obdobie profil profilové stratégie profilová stratégia navrhovaná stratégia 
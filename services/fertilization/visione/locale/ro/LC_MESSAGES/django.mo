��    <      �  S   �      (     )     H     X  [   u     �     �     �        #   	     -     ?     S     [     n       ^   �     �               >     [     r  
   �     �  &   �     �  +   �       3   3     g  )   w     �     �     �     �     �     �     
	     	      0	     Q	  	   p	     z	     �	  
   �	     �	     �	  
   �	     �	     �	     �	  !   �	     
     4
     H
     O
     W
     j
     {
  �  �
      "     C     U  t   q     �     �       	      %   *     P     e     }     �     �  #   �  d   �     ?     Q      d  &   �     �     �  
   �     �  $   �     "  ,   0     ]  ?   }     �  ,   �     �            	   ;     E     c     l     z     �  !   �     �     �     �               )     <     J     S     \  +   u  )   �     �  	   �     �     �     
                  &   6   -   +      /   7   %             1   )             :      ;                  '   $          5       ,      "                       !              8                        <      *         2       #         0                     9   .   	              3         (       
          4    Bfx coefficient, in kg N / ha. Change password Coefficient of reintegration Coefficient to modulate P and K supply to PK availability in soil. Value between 0 and 100. Documentation Harvest & residues Harvest index Humidity In kg/ha. Value between 0 and 1000. K2O concentration Kt time coefficient Log out Management console Maximum humidity Mineralisation of crop residues Mineralisation percentage of the crop residues of the preceding crop. Value between 0 and 100. Minimum humidity N concentration N concentration in in residues Nitrogen fixation by legumes Nitrogen standard dose P2O5 concentration Parameters Percentage between 0 and 100. Percentage of mass, between 0 and 100. Phosphorus (P2O5) Phosphorus (P2O5) concentration in residues Phosphorus (P2O5) standard dose Plant species identifier in the FaST Core database. Potassium (K20) Potassium (K20) concentration in residues Potassium (K2O) standard dose Profile Profile plant species Profiles Profiles plant species Requirements Standard dose Target nitrogen surplus Target phosphorus (P2O5) surplus Target potassium (K20) surplus View site Visione Welcome, efficiency efficiency coefficient has residual effect identifier label nitrogen (N) organic fertilizer organic fertilizer N efficiencies organic fertilizer N efficiency organic fertilizers period profile profile strategies profile strategy proposed strategy Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-28 18:51+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?2:1));
 Coeficientul Bfx, în kg N / ha. Schimbați parola Coeficientul de reintegrare Coeficient de modulare a aportului de P și K în funcție de disponibilitatea PK în sol. Valoare între 0 și 100. Documentație Recoltă și reziduuri Indicele de recoltă Umiditate În kg/ha. Valoare între 0 și 1000. Concentrația de K2O Coeficientul de timp Kt Deconectați-vă Consola de management Umiditate maximă Mineralizarea reziduurilor vegetale Procentul de mineralizare a reziduurilor vegetale din cultura precedentă. Valoare între 0 și 100. Umiditate minimă Concentrația de N Concentrația de N în reziduuri Fixarea azotului de către leguminoase Doza standard de azot Concentrația de P2O5 Parametrii Procent între 0 și 100. Procent din masă, între 0 și 100. Fosfor (P2O5) Concentrația de fosfor (P2O5) în reziduuri Doza standard de fosfor (P2O5). Identificatorul speciilor de plante în baza de date FaST Core. Potasiu (K20) Concentrația de potasiu (K20) în reziduuri Doza standard de potasiu (K2O) Profil Profilul speciilor de plante Profiluri Profiluri de specii de plante Cerințe Doza standard Excedent de azot țintă Excedent de fosfor (P2O5) vizat Excedent țintă de potasiu (K20) Vizualizați site-ul Visione Bine ați venit, eficiență coeficient de eficiență are efect rezidual identificator eticheta azot (N) îngrășământ organic eficiența N a îngrășămintelor organice eficiența îngrășămintelor organice N îngrășăminte organice perioadă profil strategii de profil strategie de profil strategia propusă 
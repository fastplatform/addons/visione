#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail


# Parse arguments
hasura_metadata_databases_file_path=$1
hasura_databases_directory=$(dirname $hasura_metadata_databases_file_path)
reconciled_hasura_metadata_databases_file_path=$2
yq=$3

# Function to reconcile 'functions' or 'tables' attributes of a database declared in Hasura databases metadata
function reconcile_hasura_metadata {
  reconciled_hasura_metadata_databases_file_path=$1
  database=$2
  resource_type=$3
  index_file_path=$4

  index_file_name=$(basename $index_file_path)
  index_directory_name=$(dirname $index_file_path)

  files=$(find $index_directory_name -name "*.yaml" | grep -v $index_file_name | sort)

  $yq ea '. as $table ireduce ([]; . *+ [$table])' $files | \
    $yq ea --inplace "(select(fileIndex==0).[] | select(.name == \"${database}\"))[\"${resource_type}\"] = select(fileIndex==1) | select(fileIndex==0)" \
    $reconciled_hasura_metadata_databases_file_path -
}

# Initialize the reconciled 'databases.yaml' file
$yq e $hasura_metadata_databases_file_path > $reconciled_hasura_metadata_databases_file_path

# Reconcile Hasura databases metadata
databases=$($yq e '.[].name' $reconciled_hasura_metadata_databases_file_path)
resource_types=(
  "functions"
  "tables"
)
for database in $databases; do
  for resource_type in ${resource_types[@]}; do
    index_file_path=$(
        $yq e ".[] | select(.name == \"${database}\").${resource_type}" $reconciled_hasura_metadata_databases_file_path |\
        cut -d' ' -f2
    )
    if [ "${index_file_path}" != "null" ]; then
        reconcile_hasura_metadata $reconciled_hasura_metadata_databases_file_path $database $resource_type $hasura_databases_directory/$index_file_path
    fi
  done
  $yq e --inplace \
    "(.[] | select(.name == \"${database}\")).configuration.connection_info.pool_settings.connection_lifetime = 
      \"\$(fastplatform.addons.visione.services.fertilization.visione-api-gateway.db.${database}.hasura-pg-conn-lifetime)\"" \
    $reconciled_hasura_metadata_databases_file_path
  $yq e --inplace \
    "(.[] | select(.name == \"${database}\")).configuration.connection_info.pool_settings.idle_timeout = 
      \"\$(fastplatform.addons.visione.services.fertilization.visione-api-gateway.db.${database}.hasura-pg-idle-timeout)\"" \
    $reconciled_hasura_metadata_databases_file_path
  $yq e --inplace \
    "(.[] | select(.name == \"${database}\")).configuration.connection_info.pool_settings.max_connections = 
      \"\$(fastplatform.addons.visione.services.fertilization.visione-api-gateway.db.${database}.hasura-pg-max-connections)\"" \
    $reconciled_hasura_metadata_databases_file_path
done


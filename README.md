# FaST Platform / addons / visione

Additional module that implements Italian (Piemonte) services used to calculate Nitrogen, Phosphorus and Potassium recommendations for a given fertilization plan depending on the soil type, soil samples, fertilization and rotations.

This repository includes the [source code](services) and [a descriptive orchestration configuration](manifests) of the entire package.

> The algorithm used is based on the **Visione** tool developed by the University of Torino (Laura Zavatarro).

## Services

### Fertilization

- [fertilization/visione](services/fertilization/visione)
- [fertilization/visione-api-gatewway](services/fertilization/visione_api_gateway)

### Diagram

```plantuml
title Visione services\nHigh level diagram\n

actor "Farmer/Advisor" as farmer
actor "Algorithm Admin" as admin

component "FaST Mobile app" as app #white
component "Web browser" as browser #white

component "FaST API Gateway\n//Hasura//" as fast_api #white
component "Visione API Gateway\n//Hasura//" as api
component "Visione Backend\n//Django//" as backend
database "Visione Database\n//Postgres 12//" as db

component "FaST Backend\n//Django//" as fast #white

farmer --> app
app --> fast_api : Requests NPK\nrecommendation
fast_api --> api : Authenticates and\nforwards the\nrequest
api --> backend
backend .> fast : Delegates\nauthentication

admin --> browser
backend --> db
browser --> backend : Configures\nalgorithm\nparameters
```

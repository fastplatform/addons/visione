#!/bin/bash


set -o errexit
set -o nounset
set -o pipefail

MINIMUM_KUBECTL_VERSION=v1.15.0

check_kubectl_version() {

  if ! [ -x "$(command -v kubectl)" ]; then
    echo "kubectl not in path !"
    return 1
  fi

  current_kubectl_version=$(kubectl version --client --short | awk '{print $3}')
  if [[ "${MINIMUM_KUBECTL_VERSION}" != $(echo -e "${MINIMUM_KUBECTL_VERSION}\n${current_kubectl_version}" | sort -s -t. -k 1,1 -k 2,2n -k 3,3n | head -n1) ]]; then
    cat <<EOF
Current kubectl version: ${current_kubectl_version}.
Requires ${MINIMUM_KUBECTL_VERSION} or greater.
EOF
    return 2
  fi
}

check_kubectl_version
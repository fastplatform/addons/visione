load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")
load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive", "http_file")
load("@bazel_tools//tools/build_defs/repo:git.bzl", "git_repository")

def repositories():
    maybe(
        http_archive,
        name = "io_bazel_rules_docker",
        strip_prefix = "rules_docker-0.14.4",
        urls = ["https://github.com/bazelbuild/rules_docker/releases/download/v0.14.4/rules_docker-v0.14.4.tar.gz"],
        sha256 = "4521794f0fba2e20f3bf15846ab5e01d5332e587e9ce81629c7f96c793bb7036",
    )
    maybe(
        http_archive,
        name = "rules_python",
        url = "https://github.com/bazelbuild/rules_python/releases/download/0.0.2/rules_python-0.0.2.tar.gz",
        strip_prefix = "rules_python-0.0.2",
        sha256 = "b5668cde8bb6e3515057ef465a35ad712214962f0b3a314e551204266c7be90c",
    )
    maybe(
        http_archive,
        name = "rules_python_external",
        url = "https://github.com/dillon-giacoppo/rules_python_external/archive/v0.1.5.zip",
        sha256 = "bc655e6d402915944e014c3b2cad23d0a97b83a66cc22f20db09c9f8da2e2789",
        strip_prefix = "rules_python_external-0.1.5",
    )
    maybe(
        http_archive,
        name = "rules_pkg",
        urls = [
            "https://github.com/bazelbuild/rules_pkg/releases/download/0.2.6-1/rules_pkg-0.2.6.tar.gz",
        ],
        sha256 = "aeca78988341a2ee1ba097641056d168320ecc51372ef7ff8e64b139516a4937",
    )
    maybe(
        http_file,
        name = "jq_linux",
        executable = 1,
        urls = ["https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64"],
        sha256 = "af986793a515d500ab2d35f8d2aecd656e764504b789b66d7e1a0b727a124c44",
    )
    maybe(
        http_file,
        name = "jq_osx",
        executable = 1,
        urls = ["https://github.com/stedolan/jq/releases/download/jq-1.6/jq-osx-amd64"],
        sha256 = "5c0a0a3ea600f302ee458b30317425dd9632d1ad8882259fcaf4e9b868b2b1ef",
    )
    maybe(
        http_archive,
        name = "kustomize_linux",
        urls = ["https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv3.5.4/kustomize_v3.5.4_linux_amd64.tar.gz"],
        sha256 = "5cdeb2af81090ad428e3a94b39779b3e477e2bc946be1fe28714d1ca28502f6a",
        build_file_content = "exports_files([\"kustomize\"])",
    )
    maybe(
        http_archive,
        name = "kustomize_osx",
        urls = ["https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv3.5.4/kustomize_v3.5.4_darwin_amd64.tar.gz"],
        sha256 = "9215c140593537b30e83f14277dee8a2adea9bb44825a8ed98a6c12a82fb2ea6",
        build_file_content = "exports_files([\"kustomize\"])",
    )
    maybe(
        http_file,
        name = "yq_linux",
        executable = 1,
        urls = ["https://github.com/mikefarah/yq/releases/download/v4.14.2/yq_linux_amd64"],
        sha256 = "dd2f8e4e711b434606495c16d3ffb6c33f47c5c02c72afb63b55b3741d9ef8c8",
    )
    maybe(
        http_file,
        name = "yq_osx",
        executable = 1,
        urls = ["https://github.com/mikefarah/yq/releases/download/v4.14.2/yq_darwin_amd64"],
        sha256 = "c2361d7fc6193a963dd2acb729f575ee78f07b074f2c7250921bb6698d378480",
    )

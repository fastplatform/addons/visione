import os, uvicorn

from module import app as app

config = type('obj_1', (object,), { 'LOGGING_CONFIG': None, 'LOG_LEVEL': 'info' })()

uvicorn.run(
    app = app,
    port = int(os.environ.get('PORT',8000)),
    host = os.environ.get('HOST','127.0.0.1'),
    log_config = config.LOGGING_CONFIG,
    log_level = config.LOG_LEVEL.lower(),
)